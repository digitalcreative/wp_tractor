<?php if ( has_post_thumbnail() ) { ?>
	<?php
	$tractor_thumbnail_w = 1170;
	$tractor_thumbnail_h = 740;
	?>
	<div class="post-feature post-thumbnail">
		<?php
		Tractor_Helper::get_the_post_thumbnail_url( array(
			'width'  => $tractor_thumbnail_w,
			'height' => $tractor_thumbnail_h,
			'echo'   => true,
		) );
		?>
	</div>
<?php } ?>
