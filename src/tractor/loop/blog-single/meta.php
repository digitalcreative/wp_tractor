<div class="post-meta">
	<?php if ( Tractor::setting( 'single_post_date_enable' ) === '1' ) : ?>
		<div class="post-date">
			<?php echo get_the_date(); ?>
		</div>
	<?php endif; ?>

	<?php if ( Tractor::setting( 'single_post_author_enable' ) === '1' ) : ?>
		<div class="post-author-meta">
			<span><?php echo esc_html( 'by', 'tractor' ); ?></span>
			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a>
		</div>
	<?php endif; ?>

	<?php if ( Tractor::setting( 'single_post_comment_count_enable' ) ) : ?>
		<div class="post-comments-number">
			<?php
			$comment_count = get_comments_number();
			printf( esc_html( _n( '%1$s Comment', '%1$s Comments', $comment_count, 'tractor' ) ), $comment_count );
			?>
		</div>
	<?php endif; ?>

	<?php if ( Tractor::setting( 'single_post_view_enable' ) === '1' && class_exists( 'InsightCore_View' ) ) : ?>
		<div class="post-view">
			<?php
			$views = InsightCore_View::get_views();
			printf( esc_html( _n( '%1$s View', '%1$s Views', $views, 'tractor' ) ), $views );
			?>
		</div>
	<?php endif; ?>

	<?php if ( Tractor::setting( 'single_post_like_enable' ) === '1' ) : ?>
		<?php Tractor_Templates::post_likes(); ?>
	<?php endif; ?>
</div>
