<?php
$i = 1;
while ( $tractor_query->have_posts() ) :
	$tractor_query->the_post();
	$classes = array( 'case-study-item swiper-slide' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>
		<div class="swiper-item">
			<div class="post-item-wrap">
				<div class="post-thumbnail-wrap">
					<div class="post-info">
						<div class="post-number"><?php echo Tractor_Helper::number_with_zero( $i ); ?></div>
						<div class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
						<div class="post-excerpt">
							<?php Tractor_Templates::excerpt( array(
								'limit' => 13,
								'type'  => 'word',
							) ); ?>
						</div>
						<a class="post-read-more" href="<?php the_permalink(); ?>">
							<?php esc_html_e( 'View Project', 'tractor' ); ?>
							<span class="btn-icon ion-arrow-right-c"></span>
						</a>
					</div>

					<div class="post-thumbnail"
						<?php if ( has_post_thumbnail() ) : ?>
							<?php
							$image_url = get_the_post_thumbnail_url( null, 'full' );

							if ( $image_size !== 'full' ) {
								$_sizes  = explode( 'x', $image_size );
								$_width  = $_sizes[0];
								$_height = $_sizes[1];

								$image_url = Tractor_Helper::aq_resize( array(
									'url'    => $image_url,
									'width'  => $_width,
									'height' => $_height,
									'crop'   => true,
									'echo'   => true,
									'alt'    => get_the_title(),
								) );
							}
							?>
							style="background-image: url('<?php echo esc_url( $image_url ); ?>')"
						<?php endif; ?>
					>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	$i ++;
endwhile;
