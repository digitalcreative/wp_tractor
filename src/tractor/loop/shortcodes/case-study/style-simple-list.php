<?php
while ( $tractor_query->have_posts() ) :
	$tractor_query->the_post();
	$classes = array( 'case-study-item grid-item' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>
		<a href="<?php the_permalink(); ?>">
			<div class="post-item-wrap">
				<div class="post-thumbnail-wrap">
					<a href="<?php the_permalink(); ?>">
						<div class="post-thumbnail">
							<?php if ( has_post_thumbnail() ) { ?>
								<?php
								if ( $image_size !== '' ) {
									Tractor_Helper::get_the_post_thumbnail_url( array(
										'width'  => 75,
										'height' => 64,
										'echo'   => true,
									) );
								}
								?>
							<?php } else { ?>
								<?php Tractor_Templates::image_placeholder( 480, 480 ); ?>
							<?php } ?>

						</div>
					</a>
				</div>

				<div class="post-info">
					<h5 class="post-title">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</h5>
					<div class="post-categories">
						<?php echo get_the_term_list( get_the_ID(), 'case_study_category', '', ', ', '' ); ?>
					</div>
				</div>

			</div>
		</a>
	</div>
<?php endwhile;
