<?php
extract( $tractor_shortcode_atts );
?>
<div class="content-item-wrap">

	<div class="image-wrap item-col"
		<?php if ( $image ) : ?>
			<?php
			$full_image_size = wp_get_attachment_url( $image );
			$full_image_size = Tractor_Helper::aq_resize( array(
				'url'    => $full_image_size,
				'width'  => 500,
				'height' => 588,
				'crop'   => true,
				'echo'   => true,
			) );
			?>

			style="background-image: url('<?php echo esc_url( $full_image_size ); ?>')"
		<?php endif; ?>
	></div>

	<div class="item-col">

		<div class="content">
			<?php if ( isset( ${"icon_$icon_type"} ) && ${"icon_$icon_type"} !== '' ) { ?>
				<?php
				$_args = array(
					'type'        => $icon_type,
					'icon'        => ${"icon_$icon_type"},
					'svg_animate' => isset( $icon_svg_animate_type ) ? $icon_svg_animate_type : '',
				);

				Tractor_Helper::get_vc_icon_template( $_args );
				?>
			<?php } ?>

			<div class="box-header">
				<?php if ( $heading ) : ?>
					<h4 class="heading">
						<?php
						// Item Link.
						$link = vc_build_link( $link );
						if ( $link['url'] !== '' ) {
						?>
						<a class="link-secret" href="<?php echo esc_url( $link['url'] ); ?>"
							<?php if ( $link['target'] !== '' ): ?>
								target="<?php echo esc_attr( $link['target'] ); ?>"
							<?php endif; ?>
						>
							<?php } ?>

							<?php echo esc_html( $heading ); ?>

							<?php if ( $link['url'] !== '' ) { ?>
						</a>
					<?php } ?>

					</h4>
				<?php endif; ?>
			</div>

			<?php if ( $text ) : ?>
				<?php echo '<div class="text">' . $text . '</div>'; ?>
			<?php endif; ?>

			<?php
			// Button.
			if ( $button && $button !== '' ) {
				$button = vc_build_link( $button );
				if ( $button['url'] !== '' ) {
					$button_classes = 'tm-button style-text tm-box-icon__btn';
					if ( $button_color === 'primary' ) {
						$button_classes .= ' tm-button-primary';
					} elseif ( $button_color === 'secondary' ) {
						$button_classes .= ' tm-button-secondary';
					}
					?>
					<a class="<?php echo esc_attr( $button_classes ); ?>"
					   href="<?php echo esc_url( $button['url'] ) ?>"
						<?php if ( $button['target'] !== '' ) { ?>
							target="<?php echo esc_attr( $button['target'] ); ?>"
						<?php } ?>
					>
						<span class="button-text"><?php echo esc_html( $button['title'] ); ?></span>
						<span class="button-icon ion-arrow-right-c"></span>
					</a>
				<?php }
			} ?>
		</div>

	</div>
</div>
