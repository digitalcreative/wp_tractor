<?php
while ( $tractor_query->have_posts() ) :
	$tractor_query->the_post();
	$classes = array( 'grid-item', 'post-item' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>

		<div class="post-item-wrap">
			<?php if ( has_post_thumbnail() ) { ?>
				<div class="post-feature post-thumbnail">
					<a href="<?php the_permalink(); ?>">
						<?php
						Tractor_Helper::get_the_post_thumbnail_url( array(
							'width'  => 105,
							'height' => 80,
							'echo'   => true,
						) );
						?>
					</a>
				</div>
			<?php } ?>

			<div class="post-info">

				<?php get_template_part( 'loop/blog/title' ); ?>

				<div class="post-date"><?php echo get_the_date(); ?></div>

			</div>
		</div>

	</div>
<?php endwhile;
