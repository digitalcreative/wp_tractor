<?php
while ( $tractor_query->have_posts() ) :
	$tractor_query->the_post();
	$classes = array( 'grid-item', 'post-item' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>

		<?php get_template_part( 'loop/blog/format' ); ?>

		<div class="post-info">

			<?php get_template_part( 'loop/blog/category' ); ?>

			<?php get_template_part( 'loop/blog/title' ); ?>

			<div class="post-meta">
				<div class="post-date"><?php echo get_the_date(); ?></div>

				<div class="post-author-meta">
					<?php echo esc_html__( 'by', 'tractor' ) . ' '; ?>
					<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a>
				</div>

				<div class="post-comments-number">
					<?php
					$comment_count = get_comments_number();
					printf( esc_html( _n( '%1$s Comment', '%1$s Comments', $comment_count, 'tractor' ) ), $comment_count );
					?>
				</div>

				<?php get_template_part( 'loop/blog/sticky' ); ?>
			</div>

			<div class="post-excerpt">
				<?php Tractor_Templates::excerpt( array(
					'limit' => 100,
					'type'  => 'word',
				) ); ?>
			</div>

			<div class="row row-xs-center">
				<div class="col-md-12">
					<?php get_template_part( 'loop/blog/readmore' ); ?>
				</div>
			</div>

		</div>
	</div>
<?php endwhile;
