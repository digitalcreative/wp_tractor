<?php
while ( $tractor_query->have_posts() ) :
	$tractor_query->the_post();
	$classes = array( 'post-item swiper-slide' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>
		<div class="post-item-wrap">

			<a href="<?php the_permalink(); ?>">
				<div class="post-feature-wrap">

					<?php if ( has_post_thumbnail() ) { ?>
						<div class="post-feature post-thumbnail">
							<?php
							Tractor_Helper::get_the_post_thumbnail_url( array(
								'width'  => 480,
								'height' => 480,
								'echo'   => true,
							) );
							?>
						</div>
					<?php } ?>

				</div>
			</a>

			<div class="post-info">
				<?php get_template_part( 'loop/blog/category' ); ?>

				<?php get_template_part( 'loop/blog/title' ); ?>

				<div class="post-date">
					<?php echo get_the_date( 'M d, Y' ); ?>
				</div>
			</div>
		</div>
	</div>
<?php endwhile;
