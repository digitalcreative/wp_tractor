<?php
$post_options = unserialize( get_post_meta( get_the_ID(), 'insight_post_options', true ) );
if ( $post_options !== false && isset( $post_options['post_gallery'] ) ) {
	$gallery = $post_options['post_gallery'];
	?>
	<div class="post-feature post-gallery tm-swiper" data-pagination="1" data-loop="1">
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<?php foreach ( $gallery as $image ) { ?>
					<div class="swiper-slide">
						<?php
						Tractor_Helper::get_the_post_thumbnail_url( array(
							'width'  => 1170,
							'height' => 684,
							'echo'   => true,
						) );
						?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } ?>
