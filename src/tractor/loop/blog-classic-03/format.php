<?php if ( has_post_thumbnail() ) { ?>
	<div class="post-feature post-thumbnail">
		<a href="<?php the_permalink(); ?>">
			<?php
			Tractor_Helper::get_the_post_thumbnail_url( array(
				'width'  => 400,
				'height' => 248,
				'echo'   => true,
			) );
			?>
		</a>
	</div>
<?php } ?>
