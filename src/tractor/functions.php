<?php
/**
 * Define constant
 */
$theme = wp_get_theme();

if ( ! empty( $theme['Template'] ) ) {
	$theme = wp_get_theme( $theme['Template'] );
}

if ( ! defined( 'DS' ) ) {
	define( 'DS', DIRECTORY_SEPARATOR );
}

define( 'TRACTOR_THEME_NAME', $theme['Name'] );
define( 'TRACTOR_THEME_VERSION', $theme['Version'] );
define( 'TRACTOR_THEME_DIR', get_template_directory() );
define( 'TRACTOR_THEME_URI', get_template_directory_uri() );
define( 'TRACTOR_THEME_IMAGE_DIR', get_template_directory() . DS . 'assets' . DS . 'images' );
define( 'TRACTOR_THEME_IMAGE_URI', get_template_directory_uri() . DS . 'assets' . DS . 'images' );
define( 'TRACTOR_CHILD_THEME_URI', get_stylesheet_directory_uri() );
define( 'TRACTOR_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'TRACTOR_FRAMEWORK_DIR', get_template_directory() . DS . 'framework' );
define( 'TRACTOR_CUSTOMIZER_DIR', TRACTOR_THEME_DIR . DS . 'customizer' );
define( 'TRACTOR_WIDGETS_DIR', TRACTOR_THEME_DIR . DS . 'widgets' );
define( 'TRACTOR_VC_MAPS_DIR', TRACTOR_THEME_DIR . DS . 'vc-extend' . DS . 'vc-maps' );
define( 'TRACTOR_VC_PARAMS_DIR', TRACTOR_THEME_DIR . DS . 'vc-extend' . DS . 'vc-params' );
define( 'TRACTOR_VC_SHORTCODE_CATEGORY', esc_html__( 'By', 'tractor' ) . ' ' . TRACTOR_THEME_NAME );
define( 'TRACTOR_PROTOCOL', is_ssl() ? 'https' : 'http' );

require_once TRACTOR_FRAMEWORK_DIR . '/class-static.php';

$files = array(
	TRACTOR_FRAMEWORK_DIR . '/class-init.php',
	TRACTOR_FRAMEWORK_DIR . '/class-global.php',
	TRACTOR_FRAMEWORK_DIR . '/class-debug.php',
	TRACTOR_FRAMEWORK_DIR . '/class-actions-filters.php',
	TRACTOR_FRAMEWORK_DIR . '/class-admin.php',
	TRACTOR_FRAMEWORK_DIR . '/class-customize.php',
	TRACTOR_FRAMEWORK_DIR . '/class-enqueue.php',
	TRACTOR_FRAMEWORK_DIR . '/class-functions.php',
	TRACTOR_FRAMEWORK_DIR . '/class-helper.php',
	TRACTOR_FRAMEWORK_DIR . '/class-color.php',
	TRACTOR_FRAMEWORK_DIR . '/class-import.php',
	TRACTOR_FRAMEWORK_DIR . '/class-kirki.php',
	TRACTOR_FRAMEWORK_DIR . '/class-metabox.php',
	TRACTOR_FRAMEWORK_DIR . '/class-plugins.php',
	TRACTOR_FRAMEWORK_DIR . '/class-query.php',
	TRACTOR_FRAMEWORK_DIR . '/class-custom-css.php',
	TRACTOR_FRAMEWORK_DIR . '/class-templates.php',
	TRACTOR_FRAMEWORK_DIR . '/class-aqua-resizer.php',
	TRACTOR_FRAMEWORK_DIR . '/class-visual-composer.php',
	TRACTOR_FRAMEWORK_DIR . '/class-vc-icon-ion.php',
	TRACTOR_FRAMEWORK_DIR . '/class-vc-icon-themify.php',
	TRACTOR_FRAMEWORK_DIR . '/class-vc-icon-pe-stroke-7.php',
	TRACTOR_FRAMEWORK_DIR . '/class-vc-icon-flat.php',
	TRACTOR_FRAMEWORK_DIR . '/class-vc-icon-tractor.php',
	TRACTOR_FRAMEWORK_DIR . '/class-vc-icon-icomoon.php',
	TRACTOR_FRAMEWORK_DIR . '/class-walker-nav-menu.php',
	TRACTOR_FRAMEWORK_DIR . '/class-widget.php',
	TRACTOR_FRAMEWORK_DIR . '/class-widgets.php',
	TRACTOR_FRAMEWORK_DIR . '/class-footer.php',
	TRACTOR_FRAMEWORK_DIR . '/class-post-type-blog.php',
	TRACTOR_FRAMEWORK_DIR . '/class-post-type-service.php',
	TRACTOR_FRAMEWORK_DIR . '/class-post-type-case_study.php',
	TRACTOR_FRAMEWORK_DIR . '/class-woo.php',
	TRACTOR_FRAMEWORK_DIR . '/tgm-plugin-activation.php',
	TRACTOR_FRAMEWORK_DIR . '/tgm-plugin-registration.php',
);

/**
 * Load Framework.
 */
Tractor::require_files( $files );

/**
 * Init the theme
 */
Tractor_Init::instance();

/**
 * Admin notice waning minimum plugin version required.
 *
 * @param $plugin_name
 * @param $plugin_version
 */
function tractor_notice_required_plugin_version( $plugin_name, $plugin_version ) {
	if ( isset( $_GET['activate'] ) ) {
		unset( $_GET['activate'] );
	}

	$message = sprintf(
		esc_html__( '%1$s requires %2$s plugin version %3$s or greater!', 'tractor' ),
		'<strong>' . TRACTOR_THEME_NAME . '</strong>',
		'<strong>' . $plugin_name . '</strong>',
		$plugin_version
	);

	printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );
}

/**
 * Allow to remove method for an hook when, it's a class method used and class don't have variable, but you know the class name :)
 *
 * @see https://github.com/herewithme/wp-filters-extras/blob/master/wp-filters-extras.php
 *
 * @param string $hook_name   The action hook to which the function to be removed is hooked.
 * @param string $class_name  The class name of contain function which should be removed.
 * @param string $method_name The name of the function which should be removed.
 * @param int    $priority    Optional. The priority of the function. Default 10.
 *
 * @return bool
 */
function tractor_remove_filters_for_anonymous_class( $hook_name = '', $class_name = '', $method_name = '', $priority = 10 ) {
	global $wp_filter;

	// Take only filters on right hook name and priority
	if ( ! isset( $wp_filter[ $hook_name ][ $priority ] ) || ! is_array( $wp_filter[ $hook_name ][ $priority ] ) ) {
		return false;
	}

	// Loop on filters registered
	foreach ( (array) $wp_filter[ $hook_name ][ $priority ] as $unique_id => $filter_array ) {
		// Test if filter is an array ! (always for class/method)
		if ( isset( $filter_array['function'] ) && is_array( $filter_array['function'] ) ) {
			// Test if object is a class, class and method is equal to param !
			if ( is_object( $filter_array['function'][0] ) && get_class( $filter_array['function'][0] ) && get_class( $filter_array['function'][0] ) == $class_name && $filter_array['function'][1] == $method_name ) {
				// Test for WordPress >= 4.7 WP_Hook class (https://make.wordpress.org/core/2016/09/08/wp_hook-next-generation-actions-and-filters/)
				if ( is_a( $wp_filter[ $hook_name ], 'WP_Hook' ) ) {
					unset( $wp_filter[ $hook_name ]->callbacks[ $priority ][ $unique_id ] );
				} else {
					unset( $wp_filter[ $hook_name ][ $priority ][ $unique_id ] );
				}
			}
		}

	}

	return false;
}
