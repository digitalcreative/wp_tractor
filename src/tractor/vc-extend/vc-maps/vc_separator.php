<?php

vc_add_params( 'vc_separator', array(
	array(
		'heading'     => esc_html__( 'Position', 'tractor' ),
		'description' => esc_html__( 'Make the separator position absolute with column', 'tractor' ),
		'type'        => 'dropdown',
		'param_name'  => 'position',
		'value'       => array(
			esc_html__( 'None', 'tractor' )   => '',
			esc_html__( 'Top', 'tractor' )    => 'top',
			esc_html__( 'Bottom', 'tractor' ) => 'bottom',
		),
		'std'         => '',
	),
) );
