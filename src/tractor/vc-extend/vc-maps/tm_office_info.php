<?php

class WPBakeryShortCode_TM_Office_Info extends WPBakeryShortCode {
}

vc_map( array(
	'name'     => esc_html__( 'Office Info', 'tractor' ),
	'base'     => 'tm_office_info',
	'category' => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-info-boxes',
	'params'   => array(
		array(
			'heading'     => esc_html__( 'Image', 'tractor' ),
			'type'        => 'attach_image',
			'param_name'  => 'image',
			'admin_label' => true,
		),
		array(
			'heading'     => esc_html__( 'Title', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'title',
			'admin_label' => true,
		),
		array(
			'heading'     => esc_html__( 'Address', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'address',
			'admin_label' => true,
		),
		array(
			'heading'     => esc_html__( 'Email', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'email',
			'admin_label' => true,
		),
		array(
			'heading'     => esc_html__( 'Phone', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'phone',
			'admin_label' => true,
		),
		array(
			'heading'    => esc_html__( 'Link', 'tractor' ),
			'type'       => 'vc_link',
			'param_name' => 'link',
		),
		Tractor_VC::extra_class_field(),
		Tractor_VC::get_animation_field()
	)
) );

