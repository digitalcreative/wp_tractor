<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$atts   = vc_map_get_attributes( $this->getShortcode(), $atts );
$css_id = uniqid( 'tm-jobs-box-' );
$this->get_inline_css( "#$css_id", $atts );
extract( $atts );

$jobs = (array) vc_param_group_parse_atts( $jobs );
if ( count( $jobs ) <= 0 ) {
	return;
}

$el_class  = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tm-jobs-box ' . $el_class, $this->settings['base'], $atts );

$link = vc_build_link( $link );
?>
<div class="<?php echo esc_attr( trim( $css_class ) ); ?>" id="<?php echo esc_attr( $css_id ); ?>">
	<div class="tm-jobs-box-top">
		<div class="title"><?php echo esc_html( $title ); ?></div>
		<div class="link">
			<?php
			if ( ( $link['title'] != '' ) && ( $link['url'] != '' ) ) {
				echo '<a href="' . esc_url( $link['url'] ) . '" target="' . esc_attr( $link['target'] ) . '" rel="' . esc_attr( $link['rel'] ) . '">' . esc_html( $link['title'] ) . '</a>';
			}
			?>
		</div>
	</div>
	<div class="tm-jobs-box-mid">
		<?php
		foreach ( $jobs as $job ) {
			$item_link        = vc_build_link( $job['link'] );
			$item_link_target = $item_link['target'] != '' ? $item_link['target'] : '_self';
			$item_link_rel    = $item_link['rel'] != '' ? $item_link['rel'] : 'dofollow';
			if ( $item_link['url'] != '' ) {
				echo '<a class="tm-jobs-box-item" href="' . esc_url( $item_link['url'] ) . '" target="' . esc_attr( $item_link_target ) . '" rel="' . esc_attr( $item_link_rel ) . '">';
			} else {
				echo '<div class="tm-jobs-box-item">';
			}
			if ( $job['title'] != '' ) {
				echo '<div class="title">' . esc_html( $job['title'] ) . '</div>';
			}
			if ( $job['text'] != '' ) {
				echo '<div class="text">' . esc_html( $job['text'] ) . '</div>';
			}
			if ( $item_link['url'] != '' ) {
				echo '</a>';
			} else {
				echo '</div>';
			}
		}
		?>
	</div>
</div>
