=== Tractor ===

Contributors: thememove
Tags: editor-style, featured-images, microformats, post-formats, rtl-language-support, sticky-post, threaded-comments, translation-ready

Requires at least: 4.4
Tested up to: 4.9.x
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Tractor - Industrial & Manufacturing Businesses WordPress Theme

== Description ==

Tractor brings the young and vibrant look to your website with high flexibility in customization. This is the theme of beautifully crafted pages and elements for multiple purposes.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =


== Changelog ==

= 1.9.1 - Mar 3, 2025 =
- Updated:
Insight Core plugin v.2.7.4
Updated outdated templates of WooCommerce 9.7.0

= 1.9.0 - Feb 19, 2025 =
- Updated:
Insight Core plugin v.2.7.3
Revolution Slider plugin - v.6.7.29
- Fixed:
Fix Kirki @font-face load wrong url
Improvement customize preview loading time

= 1.8.9 - Feb 7, 2025 =
- Updated :
WPBakery Page Builder plugin - v.8.2
Revolution Slider plugin - v.6.7.28

= 1.8.8 - Jan 24, 2025 =
- Fixed :
Updated outdated templates of WooCommerce 9.6.0

= 1.8.7 - Dec 31, 2024 =
- Updated :
Revolution Slider plugin - v.6.7.25
- Fixed :
Updated outdated templates of WooCommerce 9.5.0

= 1.8.6 - Dec 16, 2024 =
- Updated :
WPBakery Page Builder plugin - v.8.1
WPBakery Page Builder (Visual Composer) Clipboard plugin - v.5.0.5
Revolution Slider plugin - v.6.7.23
- Fixed :
Fix out-date WooCommerce templates v.9.4.0
Fix load theme text-domain

= 1.8.5 - October 28, 2024 =
- Updated :
Insight Core plugin 2.7.1
Revolution Slider plugin - v.6.7.20

= 1.8.4 - September 27, 2024 =
- Updated :
Revolution Slider plugin - v.6.7.19
- Fixed :
Fix out-date WooCommerce templates v.9.3.0

= 1.8.3 - September 9, 2024 =
- Updated :
Revolution Slider plugin - v.6.7.18
WPBakery Page Builder plugin - v.7.9

= 1.8.2 - August 13, 2024 =
- Updated :
Revolution Slider plugin - v.6.7.17
WPBakery Page Builder plugin - v.7.8
- Fixed :
Fix out-date WooCommerce templates v.9.0.0

= 1.8.1 - June 25, 2024 =
- Updated :
Insight Core plugin 2.7.0
Revolution Slider plugin - v.6.7.13
WPBakery Page Builder plugin - v.7.7.2
- Fixed :
Fix out-date WooCommerce templates v.9.0.0

= 1.8.0 - March 18, 2024 =
- Updated :
Insight Core plugin 2.6.7
- Fixed:
Fix google font not working error.
Fix PHP warning

= 1.7.9 - March 12, 2024 =
- Updated:
WPBakery Page Builder plugin - v.7.5
- Fixed:
Fix Scroll to ID not working

= 1.7.8 - December 27, 2023 =
- Updated:
WPBakery Page Builder plugin - v.7.3
Revolution Slider plugin - v.6.6.20

= 1.7.7 - November 8, 2023 =
- Updated:
Insight Core plugin - v.2.6.6
Revolution Slider plugin - v.6.6.18
- Fixed:
Broken plugins download link

= 1.7.6 - October 20, 2023 =
- Updated:
WPBakery Page Builder plugin - v.7.1
Revolution Slider plugin - v.6.6.16

= 1.7.5 - August 24, 2023 =
- Updated :
Insight Core plugin - v.2.6.5 (Compatible with WordPress 6.2.0)

= 1.7.4 - August 4, 2023 =
- Updated :
Revolution Slider plugin - v.6.6.15
WPBakery Page Builder plugin - v.7.0

= 1.7.3 - July 21, 2023 =
- Fixed :
Fix out-date WooCommerce templates v.7.9.0

= 1.7.2 - July 04, 2023 =
- Updated :
Revolution Slider plugin - v.6.6.14
WPBakery Page Builder plugin - v.6.13.0
WPBakery Page Builder Clipboard plugin - v.5.0.4
- Fixed:
Compatible with WooCommerce 7.8.0

= 1.7.1 - April 28, 2023 =
- Updated :
Insight Core plugin - v.2.6.4
Revolution Slider plugin - v.6.6.12
WPBakery Page Builder plugin - v.6.11.0

= 1.7.0 - February 22, 2023 =
- Updated :
Insight Core plugin - v.2.6.3
Revolution Slider plugin - v.6.6.10
- Fixed:
Compatible with WooCommerce 7.4.0

= 1.6.9 - December 20, 2022 =
- Updated :
Insight Core plugin - v.2.5.1
Revolution Slider plugin - v.6.6.7
Compatible with WooCommerce 7.2.0

= 1.6.8 - November 07, 2022 =
- Fixed:
Fix Outdated WooCommerce template files
Tested up to Compare plugin v.5.3.0

= 1.6.7 - November 01, 2022 =
- Updated :
Insight Core plugin - v.2.4.11
Revolution Slider plugin - v.6.6.5
WPBakery Page Builder plugin - v.6.10.0

= 1.6.6 - October 13, 2022 =
- Updated :
Insight Core plugin - v.2.4.10
Revolution Slider plugin - v.6.6.3
Instagram Feed plugin - v.4.0.3
Improvement code.

= 1.6.5 - May 18, 2022 =
- Updated :
Revolution Slider plugin - v.6.5.22
- Fixed:
Fix footer empty on WPML

= 1.6.4 - May 11, 2022 =
- Updated :
Insight Core plugin - v.2.4.5
Revolution Slider plugin - v.6.5.21
- Fixed:
Fix Wishlist, Compatible with WPC Smart Wishlist for WooCommerce 3.0.0

= 1.6.3 - April 13, 2022 =
- Updated :
Insight Core plugin - v.2.4.2
Revolution Slider plugin - v.6.5.20
WPBakery Page Builder plugin - v.6.9.0
WPBakery Page Builder (Visual Composer) Clipboard plugin - v.5.0.3

= 1.6.2 - March 28, 2022 =
- Updated :
Insight Core plugin - v.2.4.0
Revolution Slider plugin - v.6.5.19

= 1.6.1 - February 11, 2022 =
- Fixed:
Fix autocomplete_taxonomies_field_search in shortcode

= 1.6.0 - February 07, 2022 =
- Updated :
Insight Core plugin - v.2.2.8
Revolution Slider plugin - v.6.5.15
WPBakery Page Builder (Visual Composer) Clipboard plugin - v.5.0.2
Compatible with PHP 8.0

= 1.5.9 - December 31, 2021 =
- Updated :
Revolution Slider plugin - v.6.5.12
WPBakery Page Builder plugin - v.6.8.0

= 1.5.8 - December 07, 2021 =
- Updated:
Revolution Slider plugin - v.6.5.11
- Fixed:
Fix import demo

= 1.5.7 - November 15, 2021 =
- Fixed:
Fix PHP warning
Compatible with latest Contact Form 7.

= 1.5.6 - November 08, 2021 =
- Updated:
Revolution Slider plugin - v.6.5.9
RTL Compatible

= 1.5.5 - October 05, 2021 =
- Updated:
Insight Core plugin - v.2.2.4
Revolution Slider plugin - v.6.5.8
WPBakery Page Builder (Visual Composer) Clipboard plugin - v.5.0.1

= 1.5.4 - August 20, 2021 =
- Fixed:
Fix Contact Form 7 Ajax not working
Fix minor CSS bugs

= 1.5.2 - August 10, 2021 =
- Updated:
Revolution Slider plugin - v.6.5.6
- Added:
Instagram Feed plugin - v.4.0.2
- Improvement:
Set button color default for Compare, Wishlist.

= 1.5.1 - July 28, 2021 =
- Updated:
Revolution Slider plugin - v.6.5.5
- Fixed:
Compatibe with WooCommerce Smart Compare plugin (Requires WooCommerce Smart Compare plugin v.4.0.0 to be installed)

= 1.5.0 - July 19, 2021 =
- Updated:
Insight Core plugin - v.2.2.2
WPBakery Page Builder plugin - v.6.7.0
Revolution Slider plugin - v.6.5.4

= 1.4.8 - June 07, 2021 =
- Updated:
Insight Core plugin - v.2.2.1
Revolution Slider plugin - v.6.4.11
- Fixed:
Fix jquery migration

= 1.4.7 - May 14, 2021 =
- Updated:
Insight Core plugin - v.2.2.0
Revolution Slider plugin - v.6.4.8
WPBakery Page Builder Clipboard plugin - v.4.5.8

= 1.4.6 - March 12, 2021 =
- Updated:
Insight Core plugin - v.2.0.0
Revolution Slider plugin - v.6.4.3

= 1.0.0 - July 18, 2018 =
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
