<?php
$section  = 'social_sharing';
$priority = 1;
$prefix   = 'social_sharing_';

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'multicheck',
	'settings'    => $prefix . 'items',
	'label'       => esc_attr__( 'Sharing Links', 'tractor' ),
	'description' => esc_html__( 'Check to the box to enable social share links.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array( 'facebook', 'twitter', 'linkedin', 'google_plus' ),
	'choices'     => array(
		'facebook'    => esc_attr__( 'Facebook', 'tractor' ),
		'twitter'     => esc_attr__( 'Twitter', 'tractor' ),
		'linkedin'    => esc_attr__( 'Linkedin', 'tractor' ),
		'google_plus' => esc_attr__( 'Google+', 'tractor' ),
		'tumblr'      => esc_attr__( 'Tumblr', 'tractor' ),
		'email'       => esc_attr__( 'Email', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'sortable',
	'settings'    => $prefix . 'order',
	'label'       => esc_attr__( 'Order', 'tractor' ),
	'description' => esc_html__( 'Controls the order of social share links.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'facebook',
		'twitter',
		'google_plus',
		'tumblr',
		'linkedin',
		'email',
	),
	'choices'     => array(
		'facebook'    => esc_attr__( 'Facebook', 'tractor' ),
		'twitter'     => esc_attr__( 'Twitter', 'tractor' ),
		'google_plus' => esc_attr__( 'Google+', 'tractor' ),
		'tumblr'      => esc_attr__( 'Tumblr', 'tractor' ),
		'linkedin'    => esc_attr__( 'Linkedin', 'tractor' ),
		'email'       => esc_attr__( 'Email', 'tractor' ),
	),
) );
