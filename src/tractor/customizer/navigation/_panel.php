<?php
$panel    = 'navigation';
$priority = 1;

Tractor_Kirki::add_section( 'navigation', array(
	'title'    => esc_html__( 'Desktop Menu', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'navigation_minimal', array(
	'title'    => esc_html__( 'Off Canvas Menu', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'navigation_mobile', array(
	'title'    => esc_html__( 'Mobile Menu', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
