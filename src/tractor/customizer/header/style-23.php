<?php
$section  = 'header_style_23';
$priority = 1;
$prefix   = 'header_style_23_';

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'overlay',
	'label'    => esc_html__( 'Header Overlay', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'No', 'tractor' ),
		'1' => esc_html__( 'Yes', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'logo',
	'label'    => esc_html__( 'Logo', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'light',
	'choices'  => array(
		'light' => esc_html__( 'Light', 'tractor' ),
		'dark'  => esc_html__( 'Dark', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'sticky_logo',
	'label'    => esc_html__( 'Sticky Logo', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'sticky-light',
	'choices'  => array(
		'sticky-light' => esc_html__( 'Light', 'tractor' ),
		'sticky-dark'  => esc_html__( 'Dark', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'logo_bg',
	'label'       => esc_html__( 'Logo background Color', 'tractor' ),
	'description' => esc_html__( 'Controls the background color of logo', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#DE2021',
	'output'      => array(
		array(
			'element'  => '
			.header-23 .branding',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'search_enable',
	'label'    => esc_html__( 'Search', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'tractor' ),
		'1' => esc_html__( 'Show', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'cart_enable',
	'label'    => esc_html__( 'Mini Cart', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'tractor' ),
		'1' => esc_html__( 'Show', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'social_networks_enable',
	'label'    => esc_html__( 'Social Networks', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'tractor' ),
		'1' => esc_html__( 'Show', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'repeater',
	'settings'  => $prefix . 'link_list',
	'label'     => esc_html__( 'Link List', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'choices'   => array(
		'labels' => array(
			'add-new-row' => esc_html__( 'Add new link', 'tractor' ),
		),
	),
	'row_label' => array(
		'type'  => 'field',
		'field' => 'text',
	),
	'default'   => array(
		array(
			'text'       => 'Call Us: (+00)888.666.88',
			'url'        => 'tel:18009777880',
			'icon_class' => 'ion-ios-telephone',
		),
		array(
			'text'       => 'Email: thememove@trator.com',
			'url'        => '',
			'icon_class' => 'ion-android-map',
		),
		array(
			'text'       => 'Mon - Fri: 9:00 - 19:00 / Closed on Weekends',
			'url'        => '',
			'icon_class' => 'ion-clock',
		),
	),
	'fields'    => array(
		'text'       => array(
			'type'    => 'text',
			'label'   => esc_html__( 'Title', 'tractor' ),
			'default' => '',
		),
		'url'        => array(
			'type'    => 'text',
			'label'   => esc_html__( 'Link', 'tractor' ),
			'default' => '',
		),
		'icon_class' => array(
			'type'    => 'text',
			'label'   => esc_html__( 'Icon Class', 'tractor' ),
			'default' => '',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => $prefix . 'link_list_font_size',
	'label'       => esc_html__( 'Info list Font Size', 'tractor' ),
	'description' => esc_html__( 'Controls the font size for main info items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 14,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => '.header-23 .header-link-list .link-item',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'link_list__color',
	'label'       => esc_html__( 'Info list Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of text, link in Info list on header.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#888',
	'output'      => array(
		array(
			'element'  => '
			.header-23 .header-link-list .link-item,
			.header-23 .header-link-list .link-item a',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'link_list_hover__color',
	'label'       => esc_html__( 'Info link hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of text, link in Info list on header.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#DE2021',
	'output'      => array(
		array(
			'element'  => '.header-23 .header-link-list .link-item a:hover',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'link_list_icon__color',
	'label'       => esc_html__( 'Info icon Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of icon in Info list on header.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#DE2021',
	'output'      => array(
		array(
			'element'  => '
			.header-23 .header-link-list .link-item i',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_icon_hover_color',
	'label'       => esc_html__( 'Icon Hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color when hover of icons on header.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#DE2021',
	'output'      => array(
		array(
			'element'  => '
			.header-23 .header-social-networks a:hover,
			.header-23 .popup-search-wrap:hover i,
			.header-23 .mini-cart .mini-cart-icon:hover,
			.header-23 .page-open-mobile-menu:hover i
			',
			'property' => 'color',
		),
	),
) );

// Social

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => $prefix . 'header_social_font_size',
	'label'       => esc_html__( 'Social icon Font Size', 'tractor' ),
	'description' => esc_html__( 'Controls the font size for main info items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 16,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => '.header-23 .header-social-networks a',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'social__color',
	'label'       => esc_html__( 'Social icon Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of social icon on header.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#999',
	'output'      => array(
		array(
			'element'  => '
			.header-23 .header-social-networks a',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'social_hover__color',
	'label'       => esc_html__( 'Social icon hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of social icon hover on header.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#DE2021',
	'output'      => array(
		array(
			'element'  => '
			.header-23 .header-social-networks a:hover',
			'property' => 'color',
		),
	),
) );

// Border

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'border_width',
	'label'     => esc_html__( 'Border Bottom Width', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 0,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 50,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.header-23 .page-header-inner',
			'property' => 'border-bottom-width',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'border_color',
	'label'       => esc_html__( 'Border Bottom Color', 'tractor' ),
	'description' => esc_html__( 'Controls the border bottom color.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#eee',
	'output'      => array(
		array(
			'element'  => '
			.header-23 .page-header-inner,
			.header-23 .header-top',
			'property' => 'border-bottom-color',
		),
		array(
			'element'  => '
			.header-23 .header-link-list .link-item',
			'property' => 'border-right-color',
		),
		array(
			'element'  => '
			.header-23 .header-right-group-search > div:not(.header-search-form-wrap),
			.header-23 .header-search-form-wrap',
			'property' => 'border-left-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'box_shadow',
	'label'       => esc_html__( 'Box Shadow', 'tractor' ),
	'description' => esc_html__( 'Input box shadow for header, e.g 0 0 5px #ccc', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'output'      => array(
		array(
			'element'  => '.header-23 .page-header-inner',
			'property' => 'box-shadow',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'background',
	'settings'    => $prefix . 'background',
	'label'       => esc_html__( 'Background', 'tractor' ),
	'description' => esc_html__( 'Controls the background of header.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'background-color'      => '#fff',
		'background-image'      => '',
		'background-repeat'     => 'no-repeat',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
		'background-position'   => 'center center',
	),
	'output'      => array(
		array(
			'element' => '.header-23 .page-header-inner',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_icon_color',
	'label'       => esc_html__( 'Icon Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of icons on header.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#222',
	'output'      => array(
		array(
			'element'  => '.header-23 .page-open-mobile-menu i',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_icon_hover_color',
	'label'       => esc_html__( 'Icon Hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color when hover of icons on header.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#DE2021',
	'output'      => array(
		array(
			'element'  => '.header-23 .page-open-mobile-menu:hover i',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'cart_badge_background_color',
	'label'       => esc_html__( 'Cart Badge Background Color', 'tractor' ),
	'description' => esc_html__( 'Controls the background color of cart badge.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#DE2021',
	'output'      => array(
		array(
			'element'  => '.header-23 .mini-cart .mini-cart-icon:after',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'cart_badge_color',
	'label'       => esc_html__( 'Cart Badge Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of cart badge.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '.header-23 .mini-cart .mini-cart-icon:after',
			'property' => 'color',
		),
	),
) );

/*--------------------------------------------------------------
# Navigation
--------------------------------------------------------------*/

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Main Menu Level 1', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_background',
	'label'       => esc_html__( 'Background Color', 'tractor' ),
	'description' => esc_html__( 'Controls the background color for main menu', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '.header-23 .header-bottom',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_margin',
	'label'     => esc_html__( 'Menu Margin', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => array(
		'top'    => '0px',
		'bottom' => '0px',
		'left'   => '0px',
		'right'  => '0px',
	),
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-23 .menu__container',
			),
			'property' => 'margin',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_item_padding',
	'label'     => esc_html__( 'Item Padding', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => array(
		'top'    => '25px',
		'bottom' => '25px',
		'left'   => '15px',
		'right'  => '15px',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-23 .menu--primary .menu__container > li > a',
			),
			'property' => 'padding',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_item_margin',
	'label'     => esc_html__( 'Item Margin', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => array(
		'top'    => '0px',
		'bottom' => '0px',
		'left'   => '0px',
		'right'  => '0px',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-23  .menu--primary .menu__container > li',
			),
			'property' => 'margin',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'kirki_typography',
	'settings'    => $prefix . 'navigation_typography',
	'label'       => esc_html__( 'Typography', 'tractor' ),
	'description' => esc_html__( 'These settings control the typography for menu items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => '',
		'variant'        => '700',
		'line-height'    => '1.26',
		'letter-spacing' => '0.5px',
		'text-transform' => 'uppercase',
	),
	'output'      => array(
		array(
			'element' => '.desktop-menu .header-23 .sm-simple > li > a',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => $prefix . 'navigation_item_font_size',
	'label'       => esc_html__( 'Font Size', 'tractor' ),
	'description' => esc_html__( 'Controls the font size for main menu items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 13,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => '.header-23 .menu--primary .menu__container > li > a',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_color',
	'label'       => esc_html__( 'Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color for main menu items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#222',
	'output'      => array(
		array(
			'element'  => '
			.header-23 .wpml-ls-legacy-dropdown-click .wpml-ls-item-toggle,
			.header-23 .popup-search-wrap i,
			.header-23 .mini-cart .mini-cart-icon,
			.header-23 .page-open-main-menu,
			.header-23 .menu--primary a,
			.header-23 .header-search-form-wrap .search-submit
			',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_hover_color',
	'label'       => esc_html__( 'Hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color when hover for main menu items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#DE2021',
	'output'      => array(
		array(
			'element'  => '
			.header-23 .wpml-ls-legacy-dropdown-click .wpml-ls-item-toggle:hover,
			.header-23 .wpml-ls-legacy-dropdown-click .wpml-ls-item-toggle:focus,
			.header-23 .wpml-ls-legacy-dropdown-click .wpml-ls-current-language:hover>a,
			.header-23 .popup-search-wrap:hover i,
			.header-23 .mini-cart .mini-cart-icon:hover,
            .header-23 .menu--primary li:hover > a,
            .header-23 .menu--primary > ul > li > a:hover,
            .header-23 .menu--primary > ul > li > a:focus,
            .header-23 .menu--primary .current-menu-ancestor > a,
            .header-23 .menu--primary .current-menu-item > a,
            .header-23 .page-open-main-menu:hover,
			.header-23 .header-search-form-wrap .search-submit:hover
            ',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_background_color',
	'label'       => esc_html__( 'Background Color', 'tractor' ),
	'description' => esc_html__( 'Controls the background color for main menu items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '',
	'output'      => array(
		array(
			'element'  => '.header-23 .menu--primary .menu__container > li > a',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_hover_background_color',
	'label'       => esc_html__( 'Hover Background Color', 'tractor' ),
	'description' => esc_html__( 'Controls the background color when hover for main menu items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '',
	'output'      => array(
		array(
			'element'  => '
            .header-23 .menu--primary .menu__container > li > a:hover,
            .header-23 .menu--primary .menu__container > li.current-menu-item > a',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Header Sticky', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'background',
	'settings'    => $prefix . 'sticky_background',
	'label'       => esc_html__( 'Background', 'tractor' ),
	'description' => esc_html__( 'Controls the background of header when sticky.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'background-color'      => 'rgba(0, 0, 0, 0.8)',
		'background-image'      => '',
		'background-repeat'     => 'no-repeat',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
		'background-position'   => 'center center',
	),
	'output'      => array(
		array(
			'element' => '.header-23.headroom--not-top .page-header-inner',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'sticky_navigation_background',
	'label'       => esc_html__( 'Nav Background Color', 'tractor' ),
	'description' => esc_html__( 'Controls the background color for main menu when sticky', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => 'rgba(0, 0, 0, 0)',
	'output'      => array(
		array(
			'element'  => '.header-23.headroom--not-top .header-bottom',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'sticky_navigation_link_color',
	'label'       => esc_html__( 'Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color for main menu items when sticky.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '
			.header-23.headroom--not-top .wpml-ls-legacy-dropdown-click .wpml-ls-item-toggle,
			.header-23.headroom--not-top .popup-search-wrap i,
			.header-23.headroom--not-top .header-search-form-wrap .search-field,
			.header-23.headroom--not-top .mini-cart .mini-cart-icon,
			.header-23.headroom--not-top .page-open-mobile-menu i,
			.header-23.headroom--not-top .page-open-main-menu,
			.header-23.headroom--not-top .menu--primary > ul > li > a,
			.header-23.headroom--not-top .header-search-form-wrap .search-submit
			',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'sticky_navigation_link_hover_color',
	'label'       => esc_html__( 'Hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color when hover for main menu items when sticky.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#DE2021',
	'output'      => array(
		array(
			'element'  => '
			.header-23.headroom--not-top .wpml-ls-legacy-dropdown-click .wpml-ls-item-toggle:hover,
			.header-23.headroom--not-top .wpml-ls-legacy-dropdown-click .wpml-ls-item-toggle:focus,
			.header-23.headroom--not-top .wpml-ls-legacy-dropdown-click .wpml-ls-current-language:hover>a,
			.header-23.headroom--not-top .popup-search-wrap:hover i,
			.header-23.headroom--not-top .mini-cart .mini-cart-icon:hover,
            .header-23.headroom--not-top .menu--primary li:hover > a,
            .header-23.headroom--not-top .menu--primary > ul > li > a:hover,
            .header-23.headroom--not-top .menu--primary > ul > li > a:focus,
            .header-23.headroom--not-top .menu--primary .current-menu-ancestor > a,
            .header-23.headroom--not-top .menu--primary .current-menu-item > a,
            .header-23.headroom--not-top .page-open-main-menu:hover,
			.header-23.headroom--not-top .header-search-form-wrap .search-submit:hover
            ',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'sticky_border_color',
	'label'       => esc_html__( 'Border Bottom Color', 'tractor' ),
	'description' => esc_html__( 'Controls the border bottom color when sticky.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => 'rgba(255,255,255,0.3)',
	'output'      => array(
		array(
			'element'  => '
			.header-23.headroom--not-top .page-header-inner,
			.header-23.headroom--not-top .header-top',
			'property' => 'border-bottom-color',
		),
		array(
			'element'  => '
			.header-23.headroom--not-top .header-link-list .link-item',
			'property' => 'border-right-color',
		),
		array(
			'element'  => '
			.header-23.headroom--not-top .header-right-group-search > div:not(.header-search-form-wrap),
			.header-23.headroom--not-top .header-search-form-wrap',
			'property' => 'border-left-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'sticky_link_list__color',
	'label'       => esc_html__( 'Info list Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of text, link in Info list on header when sticky.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '
			.header-23.headroom--not-top .header-link-list .link-item,
			.header-23.headroom--not-top .header-link-list .link-item a',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'sticky_link_list_hover__color',
	'label'       => esc_html__( 'Info link hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of text, link in Info list on header when sticky.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#DE2021',
	'output'      => array(
		array(
			'element'  => '.header-23.headroom--not-top .header-link-list .link-item a:hover',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'sticky_social__color',
	'label'       => esc_html__( 'Social icon Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of social icon on header when sticky.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '
			.header-23.headroom--not-top .header-social-networks a',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'sticky_header_icon_hover_color',
	'label'       => esc_html__( 'Icon Hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color when hover of icons on header when sticky.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#DE2021',
	'output'      => array(
		array(
			'element'  => '
			.header-23.headroom--not-top .header-social-networks a:hover,
			.header-23.headroom--not-top .popup-search-wrap:hover i,
			.header-23.headroom--not-top .mini-cart .mini-cart-icon:hover,
			.header-23.headroom--not-top .page-open-mobile-menu:hover i
			',
			'property' => 'color',
		),
	),
) );
