<?php
$section  = 'header_style_07';
$priority = 1;
$prefix   = 'header_style_07_';

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'overlay',
	'label'    => esc_html__( 'Header Overlay', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'No', 'tractor' ),
		'1' => esc_html__( 'Yes', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'logo',
	'label'    => esc_html__( 'Logo', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'dark',
	'choices'  => array(
		'light' => esc_html__( 'Light', 'tractor' ),
		'dark'  => esc_html__( 'Dark', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'sticky_logo',
	'label'    => esc_html__( 'Sticky Logo', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'sticky-dark',
	'choices'  => array(
		'sticky-light' => esc_html__( 'Light', 'tractor' ),
		'sticky-dark'  => esc_html__( 'Dark', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'search_enable',
	'label'    => esc_html__( 'Search', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'tractor' ),
		'1' => esc_html__( 'Show', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'cart_enable',
	'label'    => esc_html__( 'Mini Cart', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'tractor' ),
		'1' => esc_html__( 'Show', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'social_networks_enable',
	'label'    => esc_html__( 'Social Networks', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'tractor' ),
		'1' => esc_html__( 'Show', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'language_switcher_enable',
	'label'    => esc_html__( 'Language Switcher', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'tractor' ),
		'1' => esc_html__( 'Show', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'info_enable',
	'label'    => esc_html__( 'Info', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'tractor' ),
		'1' => esc_html__( 'Show', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'repeater',
	'settings'  => $prefix . 'info',
	'label'     => esc_html__( 'Info List', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'choices'   => array(
		'labels' => array(
			'add-new-row' => esc_html__( 'Add new info', 'tractor' ),
		),
	),
	'row_label' => array(
		'type'  => 'field',
		'field' => 'title',
	),
	'default'   => array(
		array(
			'title'         => '(+00)888.666.88',
			'sub_title'     => 'Give Us a Call',
			'icon_class'    => 'ion-ios-telephone-outline',
			'link_url'      => '',
			'link_on_click' => '',
		),
		array(
			'title'         => 'home@tractor.com',
			'sub_title'     => 'Send Us a Mess',
			'icon_class'    => 'ion-ios-email-outline',
			'link_url'      => '',
			'link_on_click' => '',
		),
		array(
			'title'         => 'Mon - Fri: 09:00 - 17:00',
			'sub_title'     => 'Support 24/7',
			'icon_class'    => 'ion-ios-clock-outline',
			'link_url'      => '',
			'link_on_click' => '',
		),
		array(
			'title'         => '183 Parkways, CA, USA',
			'sub_title'     => 'Office Address',
			'icon_class'    => 'ion-ios-location-outline',
			'link_url'      => '',
			'link_on_click' => '',
		),
	),
	'fields'    => array(
		'link_url'      => array(
			'type'    => 'text',
			'label'   => esc_html__( 'Link', 'tractor' ),
			'default' => '',
		),
		'link_on_click' => array(
			'type'    => 'textarea',
			'label'   => esc_html__( 'On Click', 'tractor' ),
			'default' => '',
		),
		'title'         => array(
			'type'    => 'text',
			'label'   => esc_html__( 'Title', 'tractor' ),
			'default' => '',
		),
		'sub_title'     => array(
			'type'    => 'text',
			'label'   => esc_html__( 'Sub Title', 'tractor' ),
			'default' => '',
		),
		'icon_class'    => array(
			'type'    => 'text',
			'label'   => esc_html__( 'Icon Class', 'tractor' ),
			'default' => '',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'border_width',
	'label'     => esc_html__( 'Border Bottom Width', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 0,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 50,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.header-07 .page-header-inner',
			'property' => 'border-bottom-width',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'border_color',
	'label'       => esc_html__( 'Border Bottom Color', 'tractor' ),
	'description' => esc_html__( 'Controls the border bottom color.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#eee',
	'output'      => array(
		array(
			'element'  => '.header-07 .page-header-inner',
			'property' => 'border-bottom-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'box_shadow',
	'label'       => esc_html__( 'Box Shadow', 'tractor' ),
	'description' => esc_html__( 'Input box shadow for header, e.g 0 0 5px #ccc', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0 0 10px rgba(0, 0, 0, 0.07)',
	'output'      => array(
		array(
			'element'  => '.header-07 .page-header-inner',
			'property' => 'box-shadow',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'background',
	'settings'    => $prefix . 'background',
	'label'       => esc_html__( 'Background', 'tractor' ),
	'description' => esc_html__( 'Controls the background of header.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'background-color'      => '#fff',
		'background-image'      => '',
		'background-repeat'     => 'no-repeat',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
		'background-position'   => 'center center',
	),
	'output'      => array(
		array(
			'element' => '.header-07 .page-header-inner',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_icon_color',
	'label'       => esc_html__( 'Icon Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of icons on header.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#222',
	'output'      => array(
		array(
			'element'  => '
			.header-07 .wpml-ls-legacy-dropdown-click .wpml-ls-item-toggle,
			.header-07 .popup-search-wrap i,
			.header-07 .mini-cart .mini-cart-icon,
			.header-07 .header-social-networks a,
			.header-07 .page-open-mobile-menu i
			',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_icon_hover_color',
	'label'       => esc_html__( 'Icon Hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color when hover of icons on header.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#D0021B',
	'output'      => array(
		array(
			'element'  => '
			.header-07 .wpml-ls-legacy-dropdown-click .wpml-ls-item-toggle:hover,
			.header-07 .wpml-ls-legacy-dropdown-click .wpml-ls-item-toggle:focus,
			.header-07 .wpml-ls-legacy-dropdown-click .wpml-ls-current-language:hover>a,
			.header-07 .popup-search-wrap:hover i,
			.header-07 .mini-cart .mini-cart-icon:hover,
			.header-07 .header-social-networks a:hover,
			.header-07 .page-open-mobile-menu:hover i
			',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_info_icon_color',
	'label'       => esc_html__( 'Info Icon Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of info icons.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#D0021B',
	'output'      => array(
		array(
			'element'  => '.header-07 .header-info .info-icon',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'cart_badge_background_color',
	'label'       => esc_html__( 'Cart Badge Background Color', 'tractor' ),
	'description' => esc_html__( 'Controls the background color of cart badge.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#D0021B',
	'output'      => array(
		array(
			'element'  => '.header-07 .mini-cart .mini-cart-icon:after',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'cart_badge_color',
	'label'       => esc_html__( 'Cart Badge Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of cart badge.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '.header-07 .mini-cart .mini-cart-icon:after',
			'property' => 'color',
		),
	),
) );

/*--------------------------------------------------------------
# Navigation
--------------------------------------------------------------*/

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Main Menu Level 1', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_margin',
	'label'     => esc_html__( 'Menu Margin', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => array(
		'top'    => '0px',
		'bottom' => '0px',
		'left'   => '0px',
		'right'  => '0px',
	),
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-07 .menu__container',
			),
			'property' => 'margin',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_item_padding',
	'label'     => esc_html__( 'Item Padding', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => array(
		'top'    => '41px',
		'bottom' => '41px',
		'left'   => '14px',
		'right'  => '14px',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-07 .menu--primary .menu__container > li > a',
			),
			'property' => 'padding',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_item_margin',
	'label'     => esc_html__( 'Item Margin', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => array(
		'top'    => '0px',
		'bottom' => '0px',
		'left'   => '0px',
		'right'  => '0px',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-07  .menu--primary .menu__container > li',
			),
			'property' => 'margin',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'kirki_typography',
	'settings'    => $prefix . 'navigation_typography',
	'label'       => esc_html__( 'Typography', 'tractor' ),
	'description' => esc_html__( 'These settings control the typography for menu items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => '',
		'variant'        => '700',
		'line-height'    => '1.26',
		'letter-spacing' => '0.5px',
		'text-transform' => 'uppercase',
	),
	'output'      => array(
		array(
			'element' => '.header-07 .menu--primary .menu__container > li > a',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => $prefix . 'navigation_item_font_size',
	'label'       => esc_html__( 'Font Size', 'tractor' ),
	'description' => esc_html__( 'Controls the font size for main menu items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 13,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => '.header-07 .menu--primary .menu__container > li > a',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_color',
	'label'       => esc_html__( 'Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color for main menu items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#222',
	'output'      => array(
		array(
			'element'  => '
			.header-07 .menu--primary a
			',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_hover_color',
	'label'       => esc_html__( 'Hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color when hover for main menu items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#D0021B',
	'output'      => array(
		array(
			'element'  => '
            .header-07 .menu--primary li:hover > a,
            .header-07 .menu--primary > ul > li > a:hover,
            .header-07 .menu--primary > ul > li > a:focus,
            .header-07 .menu--primary .current-menu-ancestor > a,
            .header-07 .menu--primary .current-menu-item > a
            ',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_background_color',
	'label'       => esc_html__( 'Background Color', 'tractor' ),
	'description' => esc_html__( 'Controls the background color for main menu items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '',
	'output'      => array(
		array(
			'element'  => '.header-07 .menu--primary .menu__container > li > a',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_hover_background_color',
	'label'       => esc_html__( 'Hover Background Color', 'tractor' ),
	'description' => esc_html__( 'Controls the background color when hover for main menu items.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '',
	'output'      => array(
		array(
			'element'  => '
            .header-07 .menu--primary .menu__container > li > a:hover,
            .header-07 .menu--primary .menu__container > li.current-menu-item > a',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Button', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'button_text',
	'label'    => esc_html__( 'Button Text', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => esc_html__( 'Request a Quote', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'button_link',
	'label'    => esc_html__( 'Button link', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '#',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'button_link_target',
	'label'    => esc_html__( 'Open link in a new tab.', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'No', 'tractor' ),
		'1' => esc_html__( 'Yes', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'multicolor',
	'settings'    => $prefix . 'button_color',
	'label'       => esc_html__( 'Button Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of button.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'choices'     => array(
		'color'      => esc_attr__( 'Color', 'tractor' ),
		'background' => esc_attr__( 'Background', 'tractor' ),
		'border'     => esc_attr__( 'Border', 'tractor' ),
	),
	'default'     => array(
		'color'      => '#fff',
		'background' => '#D0021B',
		'border'     => '#D0021B',
	),
	'output'      => array(
		array(
			'choice'   => 'color',
			'element'  => '.header-07 .tm-button',
			'property' => 'color',
		),
		array(
			'choice'   => 'border',
			'element'  => '.header-07 .tm-button',
			'property' => 'border-color',
		),
		array(
			'choice'   => 'background',
			'element'  => '.header-07 .tm-button',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'multicolor',
	'settings'    => $prefix . 'button_hover_color',
	'label'       => esc_html__( 'Button Hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of button when hover.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'choices'     => array(
		'color'      => esc_attr__( 'Color', 'tractor' ),
		'background' => esc_attr__( 'Background', 'tractor' ),
		'border'     => esc_attr__( 'Border', 'tractor' ),
	),
	'default'     => array(
		'color'      => '#D0021B',
		'background' => 'rgba(0, 0, 0, 0)',
		'border'     => '#D0021B',
	),
	'output'      => array(
		array(
			'choice'   => 'color',
			'element'  => '.header-07 .tm-button:hover',
			'property' => 'color',
		),
		array(
			'choice'   => 'border',
			'element'  => '.header-07 .tm-button:hover',
			'property' => 'border-color',
		),
		array(
			'choice'   => 'background',
			'element'  => '.header-07 .tm-button:hover',
			'property' => 'background-color',
		),
	),
) );
