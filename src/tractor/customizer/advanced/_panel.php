<?php
$panel    = 'advanced';
$priority = 1;

Tractor_Kirki::add_section( 'advanced', array(
	'title'    => esc_html__( 'Advanced', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'preloader', array(
	'title'    => esc_html__( 'Pre Loader', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'light_gallery', array(
	'title'    => esc_html__( 'Light Gallery', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
