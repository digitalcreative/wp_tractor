<?php
$section  = 'single_case_study';
$priority = 1;
$prefix   = 'single_case_study_';

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'comment_enable',
	'label'       => esc_html__( 'Comments', 'tractor' ),
	'description' => esc_html__( 'Turn on to display comments on single case study posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );
