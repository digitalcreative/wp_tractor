<?php
$panel    = 'case_study';
$priority = 1;

Tractor_Kirki::add_section( 'archive_case_study', array(
	'title'    => esc_html__( 'Case Study Archive', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'single_case_study', array(
	'title'    => esc_html__( 'Case Study Single', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
