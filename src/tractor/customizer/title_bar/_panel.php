<?php
$panel    = 'title_bar';
$priority = 1;

Tractor_Kirki::add_section( 'title_bar', array(
	'title'    => esc_html__( 'General', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'title_bar_01', array(
	'title'    => esc_html__( 'Style 01', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'title_bar_02', array(
	'title'    => esc_html__( 'Style 02', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'title_bar_03', array(
	'title'    => esc_html__( 'Style 03', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'title_bar_04', array(
	'title'    => esc_html__( 'Style 04', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'title_bar_05', array(
	'title'    => esc_html__( 'Style 05', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'title_bar_06', array(
	'title'    => esc_html__( 'Style 06', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
