<?php
$section  = 'footer_06';
$priority = 1;
$prefix   = 'footer_06_';

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'kirki_typography',
	'settings'    => $prefix . 'widget_title_typo',
	'label'       => esc_html__( 'Title Font', 'tractor' ),
	'description' => esc_html__( 'These settings control the typography for title.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => Tractor::PRIMARY_FONT,
		'variant'        => '300',
		'line-height'    => '1.23',
		'letter-spacing' => '0em',
		'text-transform' => 'capitalize',
	),
	'output'      => array(
		array(
			'element' => '.footer-style-06 .widgettitle, .footer-style-06 .tm-mailchimp-form .title',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'widget_title_font_size',
	'label'     => esc_html__( 'Font size', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 30,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.footer-style-06 .widgettitle, .footer-style-06 .tm-mailchimp-form .title',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'widget_title_color',
	'label'     => esc_html__( 'Widget Title Color', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#fff',
	'output'    => array(
		array(
			'element'  => '.footer-style-06 .widgettitle, .footer-style-06 .tm-mailchimp-form .title',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'widget_title_margin_bottom',
	'label'     => esc_html__( 'Widget Title Margin Bottom', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 40,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 100,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.footer-style-06 .widgettitle, .footer-style-06 .tm-mailchimp-form .title',
			'property' => 'margin-bottom',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'text_color',
	'label'     => esc_html__( 'Text Color', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#777',
	'output'    => array(
		array(
			'element'  => '.footer-style-06, .footer-style-06 .widget_text, .footer-style-06 .tm-mailchimp-form.style-11 input[type=\'email\']',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'link_color',
	'label'     => esc_html__( 'Link Color', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#777',
	'output'    => array(
		array(
			'element'  => '
			.footer-style-06 a,
            .footer-style-06 .widget_recent_entries li a,
            .footer-style-06 .widget_recent_comments li a,
            .footer-style-06 .widget_archive li a,
            .footer-style-06 .widget_categories li a,
            .footer-style-06 .widget_meta li a,
            .footer-style-06 .widget_product_categories li a,
            .footer-style-06 .widget_rss li a,
            .footer-style-06 .widget_pages li a,
            .footer-style-06 .widget_nav_menu li a,
            .footer-style-06 .insight-core-bmw li a
			',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => $prefix . 'link_hover_color',
	'label'     => esc_html__( 'Link Hover Color', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#FCC30A',
	'output'    => array(
		array(
			'element'  => '
			.footer-style-06 a:hover,
            .footer-style-06 .widget_recent_entries li a:hover,
            .footer-style-06 .widget_recent_comments li a:hover,
            .footer-style-06 .widget_archive li a:hover,
            .footer-style-06 .widget_categories li a:hover,
            .footer-style-06 .widget_meta li a:hover,
            .footer-style-06 .widget_product_categories li a:hover,
            .footer-style-06 .widget_rss li a:hover,
            .footer-style-06 .widget_pages li a:hover,
            .footer-style-06 .widget_nav_menu li a:hover,
            .footer-style-06 .insight-core-bmw li a:hover 
			',
			'property' => 'color',
		),
	),
) );
