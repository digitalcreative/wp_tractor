<?php
$section  = 'blog_archive';
$priority = 1;
$prefix   = 'blog_archive_';

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => $prefix . 'style',
	'label'       => esc_html__( 'Blog Style', 'tractor' ),
	'description' => esc_html__( 'Select blog style that display for archive pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'grid_classic_05',
	'choices'     => array(
		'list'            => esc_html__( 'List Large Image', 'tractor' ),
		'grid_classic_01' => esc_html__( 'Grid Classic 01', 'tractor' ),
		'grid_classic_02' => esc_html__( 'Grid Classic 02', 'tractor' ),
		'grid_classic_03' => esc_html__( 'Grid Classic 03', 'tractor' ),
		'grid_classic_04' => esc_html__( 'Grid Classic 04', 'tractor' ),
		'grid_classic_05' => esc_html__( 'Grid Classic 05', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => 'blog_archive_columns',
	'label'       => esc_html__( 'Columns', 'tractor' ),
	'description' => esc_html__( 'Columns of grid style', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'xs:1;sm:2;md:2;lg:2',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'number',
	'settings' => 'blog_archive_gutter',
	'label'    => esc_html__( 'Gutter', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 30,
	'choices'  => array(
		'min'  => 0,
		'step' => 1,
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'number',
	'settings' => 'blog_archive_row_gutter',
	'label'    => esc_html__( 'Row Gutter', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 70,
	'choices'  => array(
		'min'  => 0,
		'step' => 1,
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'blog_archive_animation',
	'label'       => esc_html__( 'Animation', 'tractor' ),
	'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'fall-perspective',
	'choices'     => array(
		'none'             => esc_attr__( 'None', 'tractor' ),
		'fade-in'          => esc_attr__( 'Fade In', 'tractor' ),
		'move-up'          => esc_attr__( 'Move Up', 'tractor' ),
		'scale-up'         => esc_attr__( 'Scale Up', 'tractor' ),
		'fall-perspective' => esc_attr__( 'Fall Perspective', 'tractor' ),
		'fly'              => esc_attr__( 'Fly', 'tractor' ),
		'flip'             => esc_attr__( 'Flip', 'tractor' ),
		'helix'            => esc_attr__( 'Helix', 'tractor' ),
		'pop-up'           => esc_attr__( 'Pop Up', 'tractor' ),
	),
) );
