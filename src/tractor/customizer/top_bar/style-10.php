<?php
$section  = 'top_bar_style_10';
$priority = 1;
$prefix   = 'top_bar_style_10_';

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'social_networks_enable',
	'label'       => esc_html__( 'Social Networks', 'tractor' ),
	'description' => esc_html__( 'Controls the display the social networks', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Hide', 'tractor' ),
		'1' => esc_html__( 'Show', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'repeater',
	'settings'  => $prefix . 'link_list',
	'label'     => esc_html__( 'Link List', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'choices'   => array(
		'labels' => array(
			'add-new-row' => esc_html__( 'Add new list item', 'tractor' ),
		),
	),
	'row_label' => array(
		'type'  => 'field',
		'field' => 'text',
	),
	'default'   => array(
		array(
			'text' => 'New & Media',
			'url'  => '#',
		),
		array(
			'text' => 'Careers',
			'url'  => '#',
		),
		array(
			'text' => 'Contacts',
			'url'  => '#',
		),
	),
	'fields'    => array(
		'text' => array(
			'type'    => 'text',
			'label'   => esc_html__( 'Title', 'tractor' ),
			'default' => '',
		),
		'url'  => array(
			'type'    => 'text',
			'label'   => esc_html__( 'Link', 'tractor' ),
			'default' => '',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'padding_top',
	'label'     => esc_html__( 'Padding top', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 0,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 200,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.top-bar-10',
			'property' => 'padding-top',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'padding_bottom',
	'label'     => esc_html__( 'Padding bottom', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 0,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 200,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.top-bar-10',
			'property' => 'padding-bottom',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'kirki_typography',
	'settings'    => $prefix . 'typography',
	'label'       => esc_html__( 'Typography', 'tractor' ),
	'description' => esc_html__( 'These settings control the typography of texts of top bar section.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => '',
		'variant'        => '400',
		'line-height'    => '1.78',
		'letter-spacing' => '0em',
		'text-transform' => 'none',
	),
	'output'      => array(
		array(
			'element' => '.top-bar-10, .top-bar-10 a',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'font_size',
	'label'     => esc_html__( 'Font size', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 14,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.top-bar-10, .top-bar-10 a',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'bg_color',
	'label'       => esc_html__( 'Background', 'tractor' ),
	'description' => esc_html__( 'Controls the background color of top bar.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '.top-bar-10',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'border_width',
	'label'     => esc_html__( 'Border Bottom Width', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 1,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 50,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.top-bar-10',
			'property' => 'border-bottom-width',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'border_color',
	'label'       => esc_html__( 'Border Bottom Color', 'tractor' ),
	'description' => esc_html__( 'Controls the border bottom color of top bar.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#eee',
	'output'      => array(
		array(
			'element'  => '.top-bar-10',
			'property' => 'border-bottom-color',
		),
		array(
			'element'  => '.top-bar-10 .top-bar-office-wrapper .office .office-content-wrap',
			'property' => 'border-left-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'text_color',
	'label'       => esc_html__( 'Text', 'tractor' ),
	'description' => esc_html__( 'Controls the color of text on top bar.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#888',
	'output'      => array(
		array(
			'element'  => '.top-bar-10',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'link_color',
	'label'       => esc_html__( 'Link Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of links on top bar.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#888',
	'output'      => array(
		array(
			'element'  => '.top-bar-10 a',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'link_hover_color',
	'label'       => esc_html__( 'Link Hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color when hover of links on top bar.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#7F60D1',
	'output'      => array(
		array(
			'element'  => '.top-bar-10 a:hover, .top-bar-10 a:focus',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'social_color',
	'label'       => esc_html__( 'Social item Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of social item on top bar.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#bdbdbd',
	'output'      => array(
		array(
			'element'  => '.top-bar-10 .social-link',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'social_hover_color',
	'label'       => esc_html__( 'Social item Hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of social item hover on top bar.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#7F60D1',
	'output'      => array(
		array(
			'element'  => '.top-bar-10 .social-link:hover',
			'property' => 'color',
		),
	),
) );

// Button
Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Button', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'button_text',
	'label'    => esc_html__( 'Button Text', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => esc_html__( 'Get a quote', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'button_link',
	'label'    => esc_html__( 'Button link', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '#',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'button_link_target',
	'label'    => esc_html__( 'Open link in a new tab.', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'No', 'tractor' ),
		'1' => esc_html__( 'Yes', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'button_icon',
	'label'    => esc_html__( 'Button Icon class', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'multicolor',
	'settings'    => $prefix . 'button_color',
	'label'       => esc_html__( 'Button Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of button.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'choices'     => array(
		'color'      => esc_attr__( 'Color', 'tractor' ),
		'background' => esc_attr__( 'Background', 'tractor' ),
		'border'     => esc_attr__( 'Border', 'tractor' ),
	),
	'default'     => array(
		'color'      => '#fff',
		'background' => '#7F60D1',
		'border'     => '#7F60D1',
	),
	'output'      => array(
		array(
			'choice'   => 'color',
			'element'  => '.top-bar-10 .tm-button',
			'property' => 'color',
		),
		array(
			'choice'   => 'border',
			'element'  => '.top-bar-10 .tm-button',
			'property' => 'border-color',
		),
		array(
			'choice'   => 'background',
			'element'  => '.top-bar-10 .tm-button',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'multicolor',
	'settings'    => $prefix . 'button_hover_color',
	'label'       => esc_html__( 'Button Hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the color of button when hover.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'choices'     => array(
		'color'      => esc_attr__( 'Color', 'tractor' ),
		'background' => esc_attr__( 'Background', 'tractor' ),
		'border'     => esc_attr__( 'Border', 'tractor' ),
	),
	'default'     => array(
		'color'      => '#fff',
		'background' => '#222',
		'border'     => '#222',
	),
	'output'      => array(
		array(
			'choice'   => 'color',
			'element'  => '.top-bar-10 .tm-button:hover',
			'property' => 'color',
		),
		array(
			'choice'   => 'border',
			'element'  => '.top-bar-10 .tm-button:hover',
			'property' => 'border-color',
		),
		array(
			'choice'   => 'background',
			'element'  => '.top-bar-10 .tm-button:hover',
			'property' => 'background-color',
		),
	),
) );
