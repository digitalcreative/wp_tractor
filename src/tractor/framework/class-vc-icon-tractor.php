<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Tractor_VC_Icon_Tractor' ) ) {
	class Tractor_VC_Icon_Tractor {

		public function __construct() {
			/*
			 * Add styles & script file only on add new or edit post type.
			 */
			add_action( 'load-post.php', array( $this, 'enqueue_scripts' ) );
			add_action( 'load-post-new.php', array( $this, 'enqueue_scripts' ) );

			add_filter( 'vc_iconpicker-type-tractor', array( $this, 'add_fonts' ) );

			add_action( 'vc_enqueue_font_icon_element', array( $this, 'vc_element_enqueue' ) );

			add_filter( 'tractor_vc_icon_libraries', array( $this, 'add_to_libraries' ) );
		}

		public function add_to_libraries( $libraries ) {
			$libraries[ esc_html__( 'Tractor', 'tractor' ) ] = 'tractor';

			return $libraries;
		}

		public function vc_element_enqueue( $font ) {
			switch ( $font ) {
				case 'tractor':
					wp_enqueue_style( 'font-tractor', TRACTOR_THEME_URI . '/assets/fonts/tractor/font-tractor.css', null, null );
					break;
			}
		}

		public function enqueue_scripts() {
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
		}

		function admin_enqueue_scripts() {
			wp_enqueue_style( 'font-tractor', TRACTOR_THEME_URI . '/assets/fonts/tractor/font-tractor.css', null, null );
		}

		public function add_fonts( $icons ) {
			$new_icons = array(
				array( 'tractor-chat' => 'chat' ),
				array( 'tractor-plug' => 'plug' ),
				array( 'tractor-printer' => 'printer' ),
				array( 'tractor-attach' => 'attach' ),
				array( 'tractor-network1' => 'network1' ),
				array( 'tractor-zoom-in' => 'zoom in' ),
				array( 'tractor-email' => 'email' ),
				array( 'tractor-battery' => 'battery' ),
				array( 'tractor-brightness' => 'brightness' ),
				array( 'tractor-diagonal' => 'diagonal' ),
				array( 'tractor-upload' => 'upload' ),
				array( 'tractor-microphone' => 'microphone' ),
				array( 'tractor-padlock' => 'padlock' ),
				array( 'tractor-browser-2' => 'browser 2' ),
				array( 'tractor-retweet' => 'retweet' ),
				array( 'tractor-computer' => 'computer' ),
				array( 'tractor-swipe' => 'swipe' ),
				array( 'tractor-clock' => 'clock' ),
				array( 'tractor-headphones' => 'headphones' ),
				array( 'tractor-photo-camera' => 'photo camera' ),
				array( 'tractor-trash' => 'trash' ),
				array( 'tractor-shop' => 'shop' ),
				array( 'tractor-spider-web' => 'spider web' ),
				array( 'tractor-notification' => 'notification' ),
				array( 'tractor-smartphone' => 'smartphone' ),
				array( 'tractor-turn-right' => 'turn-right' ),
				array( 'tractor-mute' => 'mute' ),
				array( 'tractor-browser-1' => 'browser 1' ),
				array( 'tractor-bookmark' => 'bookmark' ),
				array( 'tractor-add' => 'add' ),
				array( 'tractor-close' => 'close' ),
				array( 'tractor-at' => 'at' ),
				array( 'tractor-refresh' => 'refresh' ),
				array( 'tractor-video-player' => 'video player' ),
				array( 'tractor-levels' => 'levels' ),
				array( 'tractor-share' => 'share' ),
				array( 'tractor-sort' => 'sort' ),
				array( 'tractor-play-button' => 'play button' ),
				array( 'tractor-file-1' => 'file 1' ),
				array( 'tractor-home' => 'home' ),
				array( 'tractor-browsers' => 'browsers' ),
				array( 'tractor-app' => 'app' ),
				array( 'tractor-browser' => 'browser' ),
				array( 'tractor-compass' => 'compass' ),
				array( 'tractor-file' => 'file' ),
				array( 'tractor-radar' => 'radar' ),
				array( 'tractor-picture' => 'picture' ),
				array( 'tractor-open-book' => 'open book' ),
				array( 'tractor-history' => 'history' ),
				array( 'tractor-eyeglasses' => 'eyeglasses' ),
				array( 'tractor-folder-1' => 'folder 1' ),
				array( 'tractor-cloud-computing' => 'cloud computing' ),
				array( 'tractor-volume' => 'volume' ),
				array( 'tractor-mouse' => 'mouse' ),
				array( 'tractor-push-pin' => 'push pin' ),
				array( 'tractor-favorite' => 'favorite' ),
				array( 'tractor-note' => 'note' ),
				array( 'tractor-arrows' => 'arrows' ),
				array( 'tractor-user' => 'user' ),
				array( 'tractor-network' => 'network' ),
			);

			return array_merge( $icons, $new_icons );
		}
	}

	new Tractor_VC_Icon_Tractor();
}
