<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Plugin installation and activation for WordPress themes
 */
if ( ! class_exists( 'Tractor_Register_Plugins' ) ) {
	class Tractor_Register_Plugins {

		const GOOGLE_DRIVER_API = 'AIzaSyDXOs0Bxx-uBEA4fH4fzgoHtl64g0RWv-g';

		public function __construct() {
			add_filter( 'insight_core_tgm_plugins', array( $this, 'register_required_plugins' ) );
		}

		public function register_required_plugins() {
			/*
			 * Array of plugin arrays. Required keys are name and slug.
			 * If the source is NOT from the .org repo, then source is also required.
			 */
			$plugins = array(
				array(
					'name'     => esc_html__( 'Insight Core', 'tractor' ),
					'slug'     => 'insight-core',
					'source'   => $this->get_plugin_google_driver_url( '18Ed9yJ6c8uh70wokhdZg4rxBxFw3osKR' ),
					'version'  => '2.7.4',
					'required' => true,
				),
				array(
					'name'     => esc_html__( 'WPBakery Page Builder', 'tractor' ),
					'slug'     => 'js_composer',
					'source'   => $this->get_plugin_google_driver_url( '1ndgdWRFivOfptdpaup9o29v60lvNa_oK' ),
					'version'  => '8.2',
					'required' => true,
				),
				array(
					'name'     => esc_html__( 'WPBakery Page Builder (Visual Composer) Clipboard', 'tractor' ),
					'slug'     => 'vc_clipboard',
					'source'   => $this->get_plugin_google_driver_url( '1PLS_6m5D4f7YVAeDo09ZiklRevmzg4Ee' ),
					'version'  => '5.0.5',
					'required' => false,
				),
				array(
					'name'     => esc_html__( 'Contact Form 7', 'tractor' ),
					'slug'     => 'contact-form-7',
					'required' => false,
				),
				array(
					'name'     => esc_html__( 'MailChimp for WordPress', 'tractor' ),
					'slug'     => 'mailchimp-for-wp',
					'required' => false,
				),
				array(
					'name'     => esc_html__( 'WooCommerce', 'tractor' ),
					'slug'     => 'woocommerce',
					'required' => false,
				),
				array(
					'name'     => esc_html__( 'WooCommerce Smart Quick View', 'tractor' ),
					'slug'     => 'woo-smart-quick-view',
					'required' => false,
				),
				array(
					'name'     => esc_html__( 'WooCommerce Smart Compare', 'tractor' ),
					'slug'     => 'woo-smart-compare',
					'required' => false,
				),
				array(
					'name'     => esc_html__( 'WooCommerce Smart Wishlist', 'tractor' ),
					'slug'     => 'woo-smart-wishlist',
					'required' => false,
				),
				array(
					'name'     => esc_html__( 'Revolution Slider', 'tractor' ),
					'slug'     => 'revslider',
					'source'   => $this->get_plugin_google_driver_url( '1Lqb2AQhzkl-T1jxg2GYC-viC6TKbGa3D' ),
					'version'  => '6.7.29',
					'required' => true,
				),
			);

			return $plugins;
		}

		public function get_plugin_google_driver_url( $file_id ) {
			return "https://www.googleapis.com/drive/v3/files/{$file_id}?alt=media&key=" . self::GOOGLE_DRIVER_API;
		}
	}

	new Tractor_Register_Plugins();
}
