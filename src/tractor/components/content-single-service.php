<?php
/**
 * The template part of single service posts.
 *
 * @package Tractor
 * @since   1.0
 */

$style = Tractor_Helper::get_post_meta( 'service_style', '' );
if ( $style === '' ) {
	$style = Tractor::setting( 'single_service_style' );
}
?>

<?php if ( $style === '01' ) { ?>

	<?php if ( has_post_thumbnail() ) { ?>
		<div class="post-main-feature post-main-thumbnail">
			<?php
			Tractor_Helper::get_the_post_thumbnail_url( array(
				'width'  => 1170,
				'height' => 600,
				'echo'   => true,
			) );
			?>
		</div>
	<?php } ?>

	<h2 class="post-main-title">
		<?php the_title(); ?>
	</h2>

<?php } ?>

<?php the_content(); ?>
