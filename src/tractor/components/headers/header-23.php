<header id="page-header" <?php Tractor::header_class(); ?>>
	<div id="page-header-inner" class="page-header-inner" data-sticky="1">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="header-wrap">

						<?php get_template_part( 'components/branding' ); ?>

						<div class="header-right">
							<div class="header-top">
								<?php Tractor_Templates::header_link_list(); ?>

								<?php Tractor_Templates::header_social_networks( array(
									'tooltip_enable'   => false,
									'tooltip_position' => 'bottom',
								) ); ?>
							</div>

							<div class="header-bottom">
								<?php get_template_part( 'components/navigation' ); ?>

								<div class="header-right-group-search">
									<?php Tractor_Templates::header_search_form(); ?>

									<?php Tractor_Woo::header_mini_cart(); ?>

									<?php Tractor_Templates::header_open_canvas_menu_button(); ?>

									<?php Tractor_Templates::header_open_mobile_menu_button(); ?>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<?php get_template_part( 'components/off-canvas' ); ?>

</header>
