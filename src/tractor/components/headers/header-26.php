<?php
$text = Tractor::setting( 'header_style_26_text' );
?>
<header id="page-header" <?php Tractor::header_class(); ?>>
	<div id="page-header-inner" class="page-header-inner" data-sticky="1">
		<div class="header-above">
			<?php get_template_part( 'components/branding' ); ?>

			<?php get_template_part( 'components/navigation' ); ?>

			<div class="header-right">

				<?php Tractor_Templates::header_language_switcher(); ?>
				<?php Tractor_Templates::header_button( array(
					'extra_class' => 'has-triangle-top-bottom',
				) ); ?>
				<?php Tractor_Templates::header_social_networks( array(
					'tooltip_position' => 'bottom',
				) ); ?>

				<?php Tractor_Templates::header_open_mobile_menu_button(); ?>
			</div>
		</div>

		<div class="header-below">
			<?php Tractor_Templates::header_link_list(); ?>

			<div class="header-right-below">
				<?php Tractor_Templates::header_search_form(); ?>

				<?php Tractor_Woo::header_mini_cart(); ?>

				<?php echo '<div class="header-text-wrap"><div class="header-text">' . $text . '</div></div>' ?>
			</div>
		</div>
	</div>
</header>
