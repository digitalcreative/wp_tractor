<div class="sk-image">
	<img src="<?php echo esc_url( Tractor::setting( 'preloader_image' ) ); ?>"
	     alt="<?php echo esc_attr( 'preloader', 'tractor' ); ?>"/>
</div>
