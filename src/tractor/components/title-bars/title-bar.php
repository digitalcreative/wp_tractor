<div id="page-title-bar" class="page-title-bar page-title-bar-01">
	<div class="page-title-bar-overlay"></div>

	<div class="page-title-bar-inner">
		<div class="container">
			<div class="row row-xs-center">
				<div class="col-md-12">
					<?php Tractor_Templates::get_title_bar_title(); ?>

					<?php
					$enabled = Tractor::setting( "title_bar_01_breadcrumb_enable" );

					if ( '1' === $enabled ) {
						?>
						<?php get_template_part( 'components/breadcrumb' ); ?>
						<?php
					}
					?>

				</div>
			</div>
		</div>
	</div>
</div>
