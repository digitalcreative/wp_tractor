<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue child scripts
 */
if ( ! function_exists( 'tractor_child_enqueue_scripts' ) ) {
	function tractor_child_enqueue_scripts() {
		wp_enqueue_style( 'tractor-child-style', get_stylesheet_directory_uri() . '/style.css', array( 'tractor-style' ), wp_get_theme()->get( 'Version' ) );

		// Enqueue BS Script for Dev.
		$domain = wp_parse_url( get_stylesheet_directory_uri() );
		$host   = $domain['host'];

		if ( strpos( $host, '.local' ) !== false || 'localhost' === $host ) {
			$url = sprintf( 'http://%s:3000/browser-sync/browser-sync-client.js', $host );
			$ch  = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
			$header = curl_exec( $ch );
			curl_close( $ch );
			if ( $header && strpos( $header[0], '400' ) === false ) {
				wp_enqueue_script( '__bs_script__', $url, array(), null, true );
			}
		}
	}
}
add_action( 'wp_enqueue_scripts', 'tractor_child_enqueue_scripts' );

add_filter( 'demo_options_settings', function( $settings ) {
	$op = 'blockquote:before,  .sticky span,  .comment-list .comment-actions a,  mark,  .error404 .error-404-big-title,  .page-close-mobile-menu:hover,  .growl-close:hover,  .primary-color,  .tm-button.style-flat.tm-button-primary:hover, 				.tm-button.style-outline.tm-button-primary, 				.tm-button.style-text.tm-button-primary, 				.tm-button.style-text.tm-button-primary:hover .button-icon, 				.tm-box-icon .tm-button .button-icon, 				.tm-box-icon .tm-button:hover, 				.tm-box-icon.style-1 .icon, 				.tm-box-icon.style-3 .icon, 				.tm-box-icon.style-5 .icon, 				.tm-box-icon.style-6 .icon, 				.tm-box-icon.style-9 .icon, 				.tm-contact-form-7 .form-icon, 				.tm-swiper.nav-style-3 .swiper-nav-button:hover, 				.tm-swiper.nav-style-6 .swiper-nav-button:hover, 				.tm-counter.style-01 .number-wrap, 				.tm-counter.style-02 .number-wrap, 				.tm-circle-progress-chart .chart-icon, 				.tm-maps.overlay-style-02 .middle-dot, 				.tm-product-banner-slider .tm-product-banner-btn, 				.tm-countdown.skin-dark .number, 				.tm-countdown.skin-dark .separator, 				.tm-slider-button.style-04 .slider-btn:hover, 				.tm-drop-cap.style-1 .drop-cap, 				.typed-text mark, 				.typed-text .typed-cursor, 				.tm-attribute-list.style-01 .icon, 				.tm-twitter.style-slider-quote .tweet-info:before, 				.tm-twitter.style-slider-quote .tweet-text a, 				.tm-twitter .tweet:before, 				.tm-heading.modern-with-separator .heading, 				.tm-button.tm-button-secondary.style-text:hover, 				.tm-button.style-border-text.tm-button-primary, 				.tm-button.style-border-text.tm-button-secondary .button-icon, 				.tm-case-study.style-grid .post-read-more span, 				.tm-case-study.style-grid-caption-2 .post-read-more a, 				.tm-info-boxes .box-icon, 				.tm-box-icon.style-3 .icon, 				.tm-box-icon.style-4 .icon, 				.tm-info-boxes .tm-button .button-icon, 				.tm-team-member .social-networks a:hover, 				.tm-instagram .instagram-user-name, 				.tm-blog .post-title a:hover, 				.tm-blog .post-categories a:hover, 				.tm-blog.style-list .post-author-meta a:hover, 				.tm-blog.style-02 .post-read-more .btn-icon, 				.tm-blog.style-grid_classic_02 .post-categories, 				.tm-blog.style-grid_classic_02 .post-author-meta a:hover, 				.tm-blog.style-01 .post-categories, 				.tm-blog.style-01 .post-read-more, 				.tm-blog.style-04 .post-date span, 				.tm-blog.style-metro .post-date span, 				.tm-blog.style-carousel_02 .post-read-more a, 				.tm-blog.style-carousel_02 .post-read-more .btn-icon, 				.tm-blog.style-03 .post-read-more .btn-icon, 				.tm-blog.style-05 .post-read-more, 				.tm-box-icon.style-2 .tm-box-icon__btn, 				.tm-case-study .post-title a:hover, 				.tm-case-study .post-categories:hover, 				.tm-case-study.style-simple-list .grid-item:hover .post-title, 				.tm-service .post-read-more .btn-icon, 				.tm-service-feature.style-01 .icon, 				.tm-service.style-04 .post-icon, 				.tm-service.style-05 .post-icon, 				.tm-office-info .link a:after, 				.tm-office-info .info > div:before, 				.tm-category-feature.style-01 .icon, 				.tm-product.style-grid .woosw-btn.woosw-added, 				.tm-pricing .feature-icon, 				.tm-pricing.style-2 .price-wrap-inner, 				.tm-pricing-rotate-box .tm-pricing-list li:before, 				.tm-service-pricing-menu .service-cost, 				.tm-testimonial.style-3 .testimonial-by-line, 				.tm-swiper.nav-style-7 .swiper-nav-button:hover, 				.tm-list .marker, 				.tm-list .link:hover, 				.tm-list.style-modern-icon-04 .marker, 				.tm-social-networks .link:hover, 				.woosw-area .woosw-inner .woosw-content .woosw-content-top .woosw-close:hover, 				.woosw-area .woosw-inner .woosw-content .woosw-content-bot .woosw-content-bot-inner .woosw-page a:hover, 				.woosw-continue:hover, 				.skin-primary .wpcf7-text.wpcf7-text, .skin-primary .wpcf7-textarea, 				.tm-menu .menu-price, 				.page-content .tm-custom-menu.style-1 .menu a:hover, 				.post-share a:hover, 				.post-share-toggle, 				.single-post .post-meta .sl-icon, 				.single-post .entry-banner .post-meta a:hover, 				.post-share .post-share-title:before, 				.single-post .post-tags span:before, 				.related-posts .related-post-title a:hover, 				.single-case_study .entry-banner .post-categories,.simple-footer .social-networks a:hover,.widget_recent_entries .post-date:before,.tm-mailchimp-form.style-1 input[type=\'email\']:focus,.tm-mailchimp-form.style-3 .form-submit,.tm-mailchimp-form.style-5 .form-submit:hover,.tm-mailchimp-form.style-6 .form-submit,.tm-mailchimp-form.style-11 .form-submit:hover:after,.page-sidebar-fixed .widget a:hover,.top-bar-office-wrapper .office-list a:hover,.menu--primary .menu-item-feature,.nav-links a:hover:after,.page-main-content .search-form .search-submit:hover .search-btn-icon,.widget_search .search-submit:hover .search-btn-icon, .widget_product_search .search-submit:hover .search-btn-icon,.tm-testimonial.style-8.pagination-style-8 .swiper-pagination .swiper-pagination-current,.tm-cta-box .info .link a,.tm-popup-video.style-button-05 a .video-play,.tm-case-study.style-carousel-2 .post-info .post-number,.tm-case-study.style-carousel-2 .post-info .post-read-more:after {color: $value !important}';
	$op .= '.primary-background-color,
                .hint--primary:after,
                .page-scroll-up,
                .widget_calendar #today,
                .top-bar-01 .top-bar-button,
                .desktop-menu .header-09 .header-special-button,
                .tm-accordion.style-1 .accordion-title:before,
                .tm-accordion.style-2 .accordion-title:after,
                .tm-accordion.style-3 .active .accordion-title,
                .tm-accordion.style-3 .accordion-title:hover,
				.tm-maps.overlay-style-01 .animated-dot .middle-dot,
				.tm-maps.overlay-style-01 .animated-dot div[class*=\'signal\'],
				.tm-button.style-border-text .button-text:after,
				.tm-button.style-outline.tm-button-grey:hover,
				.vc_tta.vc_general.vc_tta-style-tractor-tour-01 .vc_tta-tabs-list .vc_tta-tab:hover, .vc_tta.vc_general.vc_tta-style-tractor-tour-01 .vc_tta-tabs-list .vc_tta-tab.vc_active,
				.vc_tta.vc_general.vc_tta-style-tractor-tour-02 .vc_tta-tabs-list .vc_tta-tab:hover, .vc_tta.vc_general.vc_tta-style-tractor-tour-02 .vc_tta-tabs-list .vc_tta-tab.vc_active,
				.tm-gallery-slider .lSAction .lSPrev:hover .nav-button-icon:before, .tm-gallery-slider .lSAction .lSNext:hover .nav-button-icon:before,
				.tm-card.style-2 .icon:before,
				.tm-gallery .overlay,
				.tm-grid-wrapper .btn-filter:after,
				.tm-grid-wrapper .filter-counter,
				.related-posts .related-post-date,
				.tm-blog.style-list .post-quote,
				.tm-blog.style-list .post-categories a:hover,
				.tm-blog.style-list .post-read-more a,
				.tm-blog.style-02 .post-date,
				.tm-blog.style-grid_classic_02 .post-date,
				.tm-blog.style-grid_classic_02 .post-categories a:hover,
				.tm-blog.style-01 .post-read-more:hover,
				.tm-blog.style-04 .post-categories a,
				.tm-blog.style-05 .post-read-more:after,
				.tm-blog.style-metro .post-categories a,
				.tm-blog.style-03 .post-categories a,
				.tm-office-info .link a:before,
				.tm-page-feature.style-01 .grid-item.current .post-item-wrap, .tm-page-feature.style-01 .grid-item:hover .post-item-wrap,
				.tm-case-study.style-grid .post-thumbnail-wrap:hover .post-read-more,
				.tm-case-study.style-grid-caption-2 .post-read-more a:hover,
				.tm-case-study.style-simple-list .post-item-wrap:before,
				.tm-case-study.style-carousel-3 .post-info,
				.tm-service.style-01 .post-info:after,
				.tm-service.style-02 .post-thumbnail-wrap:after,
				.tm-service.style-05 .post-read-more,
				.tm-service-feature.style-01 .current .post-item-wrap,
				.tm-service-feature.style-01 .grid-item:hover .post-item-wrap,
				.tm-category-feature.style-01 .current .cat-item-wrap,
				.tm-category-feature.style-01 .grid-item:hover .cat-item-wrap,
				.tm-drop-cap.style-2 .drop-cap,
				.tm-box-icon.style-2 .tm-box-icon__btn:after,
				.tm-box-icon.style-5 .tm-box-icon__btn:hover,
				.tm-box-icon.style-7 .tm-box-icon__btn:after,
				.tm-box-icon.style-8 .content-wrap a.tm-button:before,
				.tm-icon.style-01 .icon,
				.tm-contact-form-7.style-02 .wpcf7-submit:hover,
				.tm-mailchimp-form.style-2 .form-submit,
				.tm-mailchimp-form.style-9 .form-submit:hover,
				.tm-card.style-1,
				.tm-list.style-modern-icon-02 .marker,
				.tm-rotate-box .box,
				.tm-social-networks.style-solid-rounded-icon .item:hover .link,
				.tm-social-networks.style-solid-rounded-icon-02 .item:hover .link,
				.tm-separator.style-thick-short-line .separator-wrap,
				.tm-button.style-flat.tm-button-primary,
				.tm-button.style-outline.tm-button-primary:hover,
				.tm-button.style-border-icon,
				.tm-button.style-modern,
				.tm-callout-box.style-01,
				.tm-heading.thick-separator .separator:after,
				.tm-heading.modern-with-separator-02 .heading:after,
				.tm-gradation .count-wrap:before, .tm-gradation .count-wrap:after,
				.vc_progress_bar .vc_general.vc_single_bar .vc_bar,
				.tm-swiper .swiper-nav-button:hover,
				.tm-swiper .swiper-pagination-bullet:hover:before,
				.tm-swiper .swiper-pagination-bullet.swiper-pagination-bullet-active:before,
				.tm-pricing.style-1 .price-wrap,
				.tm-testimonial.style-3 .quote-icon,
				.tm-testimonial.style-4 .swiper-custom-btn:hover,
				.tm-testimonial-list .quote-icon,
				.tm-timeline.style-01 .content-header,
				.tm-timeline.style-01 .dot:after,
				.tm-gradation .dot:after,
				.tm-slider-button.style-02 .slider-btn:hover,
				.tm-grid-wrapper .tm-filter-button-group-inner,
				.wpb-js-composer .vc_tta.vc_general.vc_tta-style-tractor-01 .vc_tta-tab.vc_active > a,
				.wpb-js-composer .vc_tta.vc_general.vc_tta-style-tractor-01 .vc_tta-tab:hover > a,
				.wpb-js-composer .vc_tta.vc_general.vc_tta-style-tractor-01 .vc_active .vc_tta-panel-heading,
				.post-share .post-share-list a:hover,
				.single-post .post-categories a:hover,
				.page-sidebar .widget_pages .current-menu-item,
				.page-sidebar .widget_nav_menu .current-menu-item,
				.page-sidebar .insight-core-bmw .current-menu-item,
				.post-type-service .page-sidebar .widget_pages .current-menu-item,
				.post-type-service .page-sidebar .widget_nav_menu .current-menu-item,
				.post-type-service .page-sidebar .insight-core-bmw .current-menu-item,
				.page-sidebar .widget_pages a:hover,
				.page-sidebar .widget_nav_menu a:hover,
				.page-sidebar .insight-core-bmw a:hover,
				.widget_archive a:hover,
				.widget_categories a:hover,
				.widget_categories .current-cat-ancestor > a,
				.widget_categories .current-cat-parent > a,
				.widget_categories .current-cat > a,
				.tm-rotate-box.style-2 .heading:after,
				.tagcloud a:hover,
				.single-post .post-tags a:hover,
				.tm-search-form .category-list a:hover,
				.select2-container--default .select2-results__option--highlighted[aria-selected],
				.tm-heading.with-separator-04:before,
				.tm-service.style-carousel_05 .service-item:nth-child(2n+1) .post-info,
				.tm-slider-button.style-03 .slider-btn:hover,
				.tm-heading.with-separator-03 .heading:before, .tm-heading.with-separator-03 .heading:after,
				.box-bg-primary .vc_column-inner .wpb_wrapper,
				.tm-swiper.pagination-style-11 .swiper-pagination-bullet.swiper-pagination-bullet-active,
				.tm-swiper.pagination-style-11 .swiper-pagination-bullet:hover,
				.tm-jobs-box .tm-jobs-box-top .link a:hover:after,
				.tm-social-networks.style-rounded-icon-title .item:hover .link-icon,
				.tm-case-study.style-carousel-2 .tm-swiper .swiper-nav-button:hover,
				.tm-service.style-carousel_06 .service-item:nth-child(3n+3) .post-info  {background-color: $value !important}';

	return array_merge( $settings, array(
		array(
			'id'       => 'tm_builderplus_custom',
			'label'    => 'Custom',
			'settings' => array(
				array(
					'settings'  => 'primary_color',
					'type'      => 'colorpattern',
					'label'     => 'Predefined Color',
					'transport' => 'postMessage',
					'default'   => '#003874',
					'choices'   => array(
						'#003874' => '#003874',
						'#FFB700' => '#FFB700',
						'#BB2A2A' => '#BB2A2A',
						'#ED7701' => '#ED7701',
						'#96C04A' => '#96C04A',
						'#B25900' => '#B25900',
						'#1E92A3' => '#1E92A3',
					),
					'output'    => $op,
				),
				array(
					'settings'  => 'primary_color',
					'type'      => 'color',
					'label'     => 'Color Picker',
					'transport' => 'postMessage',
					'default'   => '#003874',
					'value'     => '#003874',
					'output'    => $op,
				),
				array(
					'settings'  => 'box_mode',
					'type'      => 'buttonset',
					'label'     => 'Layout',
					'transport' => 'reload',
					'reload'    => 'param',
					'default'   => '0',
					'choices'   => array(
						'1' => 'Boxed',
						'0' => 'Wide',
					),
				),
				array(
					'settings'  => 'background_pattern',
					'type'      => 'imagepattern',
					'label'     => 'Background Pattern',
					'transport' => 'postMessage',
					'default'   => '',
					'choices'   => array(
						DEMO_OPTIONS_URL . 'assets/patterns/1.png'  => DEMO_OPTIONS_URL . 'assets/patterns/1.png',
						DEMO_OPTIONS_URL . 'assets/patterns/2.png'  => DEMO_OPTIONS_URL . 'assets/patterns/2.png',
						DEMO_OPTIONS_URL . 'assets/patterns/3.png'  => DEMO_OPTIONS_URL . 'assets/patterns/3.png',
						DEMO_OPTIONS_URL . 'assets/patterns/4.png'  => DEMO_OPTIONS_URL . 'assets/patterns/4.png',
						DEMO_OPTIONS_URL . 'assets/patterns/5.png'  => DEMO_OPTIONS_URL . 'assets/patterns/5.png',
						DEMO_OPTIONS_URL . 'assets/patterns/6.png'  => DEMO_OPTIONS_URL . 'assets/patterns/6.png',
						DEMO_OPTIONS_URL . 'assets/patterns/7.png'  => DEMO_OPTIONS_URL . 'assets/patterns/7.png',
						DEMO_OPTIONS_URL . 'assets/patterns/8.png'  => DEMO_OPTIONS_URL . 'assets/patterns/8.png',
						DEMO_OPTIONS_URL . 'assets/patterns/9.png'  => DEMO_OPTIONS_URL . 'assets/patterns/9.png',
						DEMO_OPTIONS_URL . 'assets/patterns/11.png' => DEMO_OPTIONS_URL . 'assets/patterns/11.png',
						DEMO_OPTIONS_URL . 'assets/patterns/12.png' => DEMO_OPTIONS_URL . 'assets/patterns/12.png',
						DEMO_OPTIONS_URL . 'assets/patterns/13.png' => DEMO_OPTIONS_URL . 'assets/patterns/13.png',
					),
					'output'    => 'body {background-image: url("$value"); background-size:auto; background-repeat: repeat; background-attachment: fixed;}',
				),
				array(
					'settings'  => 'background_pattern',
					'type'      => 'imagepattern',
					'label'     => 'Background Image',
					'transport' => 'postMessage',
					'default'   => '',
					'choices'   => array(
						DEMO_OPTIONS_URL . 'assets/patterns/bg1.jpg' => DEMO_OPTIONS_URL . 'assets/patterns/bg1.jpg',
						DEMO_OPTIONS_URL . 'assets/patterns/bg2.jpg' => DEMO_OPTIONS_URL . 'assets/patterns/bg2.jpg',
						DEMO_OPTIONS_URL . 'assets/patterns/bg3.jpg' => DEMO_OPTIONS_URL . 'assets/patterns/bg3.jpg',
						DEMO_OPTIONS_URL . 'assets/patterns/bg4.jpg' => DEMO_OPTIONS_URL . 'assets/patterns/bg4.jpg',
					),
					'output'    => 'body {background-image: url("$value"); background-size:cover; background-repeat: no-repeat; background-attachment: fixed;}',
				),
			),
		),
	) );
} );
