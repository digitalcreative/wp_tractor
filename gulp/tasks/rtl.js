'use strict';
var gulp       = require( 'gulp' ),
    $          = require( 'gulp-load-plugins' )(),
    processors = require( '../processors' ),
    path       = require( '../paths' ).root.main;

// Build rtl.css.
gulp.task( 'rtl', function() {
	return gulp.src( [
		path + 'style.css',
		path + 'style.min.css'
	] )
	           .pipe( $.rtlcss() )
	           .pipe( $.rename( { suffix: '-rtl' } ) )
	           .pipe( $.rename( function( opt ) {
		           opt.basename = opt.basename.replace( '.min-rtl', '-rtl.min' );
		           return opt;
	           } ) )
	           .pipe( gulp.dest( path ) );
} );
